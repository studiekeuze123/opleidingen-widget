# A messy collection of data that could be useful

http://www.odata.org/getting-started/basic-tutorial/#expand


https://www.elastic.co/guide/en/elasticsearch/reference/current/disk-allocator.html
cluster.routing.allocation.disk.threshold_enabled


#### Access token

method
: POST

uri
: https://token-acceptatie.studiekeuzedatabase.nl/token
: https://token.studiekeuzedatabase.nl/token

header
: Content-Type: application/x-www-form-urlencoded

payload
: grant_type: client_credentials
: client_id: Test
: client_secret: VN5/YG8lI8uo76wXP6tC+39Z1Wzv+XTI/bc0LPLP40U=

response JSON
: access_token
: token_type
: expires_in


#### Refresh token
882234dc0c13461285640ddbc25c233e
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6IlRlc3QiLCJyb2xlIjpbIkFQSSBDb3Vyc2VzIiwiQVBJIEdlYnJ1aWtlciIsIkFQSSBJbnN0aXR1dGlvbnMiLCJBUEkgT3BlbiBkYWdlbiIsIkFQSSBPcGxlaWRpbmdlbmJvb20iXSwiQWxsb3dlZFJvdXRlIjpbImFjY3JlZGl0YXRpb25zIiwiYXBwbGljYXRpb25yZXF1aXJlbWVudHMiLCJpbnN0cnVjdGlvbmxhbmd1YWdlcyIsImluc3RydWN0aW9ubW9kZXMiLCJwcm9ncmFtZm9ybXMiLCJwcm9ncmFtcnVucyIsInByb2dyYW1zdGFydHMiLCJzY2hvbGFyc2hpcHMiLCJzZWFyY2h3b3JkcyIsInN0dWR5YWR2aWNlcyIsInN0dWR5Y29zdHMiLCJ0cmFja3MiLCJ0dWl0aW9uZmVlcyIsImNvdXJzZXMiLCJjb3Vyc2Vjb250YWN0cyIsImNvdXJzZWxhbmd1YWdlc3RyaW5ncyIsIm9yZ3VuaXRsb2NhdGlvbnMiLCIkbWV0YWRhdGEiLCJpbnN0aXR1dGlvbnMiLCJjb250YWN0ZGF0YSIsImluc3RpdHV0aW9ubGFuZ3VhZ2VzdHJpbmdzIiwib3BlbmRhZ2VuIiwib3BlbmRhZ2RvZWxncm9lcG5pdmVhdXMiLCJvcGVuZGFnZmFzZXMiLCJvcGVuZGFnbG9jYXRpZXMiLCJvcGVuZGFnbml2ZWF1b3BsZWlkaW5nZW4iLCJvcGVuZGFnb3BsZWlkaW5nZW4iLCJnZW1lZW50ZW4iLCJpbnN0ZWxsaW5nZW4iLCJpbnN0ZWxsaW5nb3BsZWlkaW5nZW4iLCJvcGxlaWRpbmdlbiIsIm9wbGVpZGluZ3N2b3JtZW4iLCJzdHVkaWVzIiwidmVzdGlnaW5nZW4iLCJiZXJvZXBlbiIsImNvbGxlZ2VnZWxkZW4iLCJkb29yc3Ryb29tIiwidmFyaWFudGVuIiwiYWZzdHVkZWVycmljaHRpbmdlbiJdLCJpc3MiOiJodHRwczovL3Rva2VuLWFjY2VwdGF0aWUuc3R1ZGlla2V1emVkYXRhYmFzZS5ubCIsImF1ZCI6IjU3OWQ3OGM3ODJiMzQ3Mjc5OWM5ZTFmNmUwNDU1NzQ4IiwiZXhwIjoxNDYyODgwODEwLCJuYmYiOjE0NjI4NzkwMTB9.lmJSJcJMgqmPSsieFwQJgZisZThBeXKa2zwJf7XER2o

method
: POST

uri
: https://token-acceptatie.studiekeuzedatabase.nl/token
: https://token.studiekeuzedatabase.nl/token

header
: Content-Type: application/x-www-form-urlencoded

payload:
: grant_type: refresh_token
: refresh_token: e7e4f1415c9f4ba79919bfbe1bdcda40
: client_id: Test

response JSON:
: access_token
: token_type
: expires_in


#### 500

JSON:
: error: Het soort fout dat opgetreden is
: error_description: De foutmelding zelf


#### Request // todo: untested/unverified

method
: POST
uri
: https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc
: https://api.studiekeuzedatabase.nl/DatamartService.svc
endpoints
: Instellingen
: Instellingen?$orderby=Instellingsnaam
header
: Content-Type: application/x-www-form-urlencoded
payload ????????????????????????????????????????????????????
: access_token: e7e4f1415c9f4ba79919bfbe1bdcda40
: client_id: Test
response JSON
: *


### old Hodex REST server

uri: http://skwebapi2.test.qdelft.nl
endpoint: /hodexDirectory
headers: X-AccessKey: 871C775D635C6FE39753C6703BFD0A2D480B86738A0D4B1A78042C6B7875B35EF09E26B314EA641BD75A2A03C334DE4B792E12E1B6E47A2151BC1B5AB0893FCE

#### old flow
```
hodexDirectory
  └ organisation/[name]/hodexDirectory...        -> es import orgUnitDataResourceURL
      │                                                   id: orgUnitId
      │                                                   type: organisation
      │                                                   index: index
      │                                                   doc: orgUnitDataResourceURL
      ├  organisation/[name]/program/[index]...  -> es import hodexResourceURL
      │                                                   id: orgUnitId-programId
      │                                                   type: program
      │                                                   index: index
      │                                                   doc: hodexResourceURL
      └  organisation/[name]/eventCalendar        -> es import calendarResourceURL->event...
                                                          id: orgUnitId-eventId
                                                          type: calendar
                                                          index: index
                                                          doc: event
```

## Miscelaneous

 - index XML: https://groups.google.com/forum/#!topic/elastica-php-client/VkcCa72BFKY
 - Elasticsearch XML Plugin: https://github.com/jprante/elasticsearch-xml
 - jsdoc@3.3.0-alpha9 --- see: https://github.com/jsdoc3/jsdoc/issues/510

