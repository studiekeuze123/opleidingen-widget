# Studiekeuze123 Opleidingen Widget

Studiekeuze123 provides data on courses and organisations through an easy implementable widget. The data comes from an Elasticsearch server.
This document describes how to make frontend and backend changes and how to setup the Elasticsearch server.

1. [Project setup](#project-setup)
2. [Frontend implementation](#frontend-implementation)
3. [The Elasticsearch import](#the-elasticsearch-import)
4. [Elasticsearch](#elasticsearch)

_______________________________________________________________

## Project setup
This project uses NodeJS (npm), Elasticsearch, Bower and JSDoc. These are not added to CVS and have to be installed individually, but not all are required:

* build processes are done through [NodeJS](https://nodejs.org/en/)
* an [Elasticsearch](https://www.elastic.co/) installation is only needed if you want to test Elasticsearch locally
* [Bower](http://bower.io/) is needed if you want to make source-code changes
* [JSDoc](https://github.com/jsdoc3/jsdoc/) is needed if you want to update documentation.

### NodeJS

Unless you've already done so, [install NodeJS and npm](https://docs.npmjs.com/getting-started/installing-node).
Then, use the command line to install Bower: `npm install -g bower`.

### Dependencies

To get started you must first get the dependencies.
Get the development dependencies using cli `npm install`.
Get the front-end dependencies using cli `bower install`.

### Tasks

Build tasks are all accessible through `npm run [taskname]`.
You can check `package.json/scripts` for all available tasks.

The most important tasks are the following:
 - `npm run build` builds the actual widget from HTML, CSS and Javascript sources
 - `npm run doc` creates documentation into `./doc/`
 - `npm run elasticsearch` runs a local Elasticsearch server
 - `npm run elasticsearch:import` imports oData into the Elasticseach server

### Task configuration

The Elasticsearch tasks can be configured by adding a configuration file. A config file for local development is already in place: `./task/.config.local.js`.
Say you want to do an import on a live server you can copy this config file to say: `./task/.config.live.js` and change the required fields.
Your subsequent import call would then become:
`npm run elasticsearch:import -- --config=live`

#### Versioning

`npm run version`
`npm run version -- --major=2`
`npm run version -- --release=beta --patch`

_______________________________________________________________

## Frontend implementation

The widget uses Bootstrap CSS, since use of the framework is widespread and the framework is solid, responsive and easy to modify. The required files are loaded asynchronously through CDN but can be overridden through the options.

### Build process

The widget is composed of one single Javascript file. The file contains the CSS and HTML for the widget.
So besides concatenating and minifying dependencies the build process is needed to process and insert the CSS into the Javascript.
The build process is as follows:

 - convert LESS to CSS
 - convert widget HTML template
 - insert CSS and HTML into main Javascript file
 - convert .po files to Javascript localisation files
 - concatenate and minify all required Javascript files

### Localisation

The default language (nl) is included in the widget. Other languages can be included through the widget options. The necessary files are generated from (.po files)[https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html] located in the `lang` folder. These can be changed with an editor like (Poedit)[http://poedit.net/].

New .po files can be created by using the `lang/sk123ow.pot` template file. When you create a .po file `sk123ow-ru.po` and run `npm run lang` a new Javascript file is created inside the source folder `src/js/sk123ow.lang.ru.js`.

This new file should be loaded separately alongside `sk123ow.min.js` and in the initialisation call the lang option should be set accordingly.

### Key generation

To allow the widget to run only on specified domains there is a key generator. This is a simple HTML file that can run in any browser without having to run a server. You can also run it through cli by `npm run serve:keygen`.

The generator uses [MurmurHash](https://en.wikipedia.org/wiki/MurmurHash) and [Base64](https://en.wikipedia.org/wiki/Base64) to generate a single hash for a number of domains.

_______________________________________________________________

## The Elasticsearch import

All data for the widget is accessible through an oData API. We use the API to populate an Elasticsearch server.

### REST API

The API uses oAuth2 for authentication, so `task/elasticsearchImport.js` will first request an access-token to be used in subsequent calls.

Origin and endpoint for the token are:
`https://token-acceptatie.studiekeuzedatabase.nl/token`

Origin and endpoint for the API are:
`https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc`

### Purging Elasticsearch

While requesting the token the import script will also try to purge the existing Elasticsearch index.

After that all the data for the **Institutions** and **Courses** is requested. The results are then imported into Elasticsearch as **organisation** and **program** respectively.
Say **Institutions** yields a collection of 50 results, then each of these is imported as type **organisation** separately.

_______________________________________________________________


## Elasticsearch

[Elasticsearch](http://www.elasticsearch.org/) is an open-source search server. It provides a distributed, multitenant-capable full-text search engine with a RESTful web interface and schema-free JSON documents. The widget frontend uses the Javascript API: elasticsearch-js. Here are some links:

 - [Getting started](http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/getting-started.html)
 - [Queries](http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html)
 - [Elasticsearch-js](https://github.com/elasticsearch/elasticsearch-js)

### Local implementation

To run Elasticsearch locally simply download it from their site and put it somewhere in the project root. Best is to read the [Getting started guide](http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/getting-started.html) but for quick use do the following:

#### Configuration

Elasticsearch will run out-of-the-box but recommends doing the following config changes:

Open config file located at: `[elasticsearch-folder]/config/elasticsearch.yml`.
Change the [cluster name](http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/setup-configuration.html#cluster-name): `cluster.name: studiekeuze123`
Change the [node name](http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/setup-configuration.html#node-name): `node.name: "Franz Kafka"`
Set http CORS:
http.cors.enabled: true
http.cors.allow-origin: "*"

#### Run

If you install any Elasticsearch version other than 2.3.2 you will have to edit `./task/.config.local.js` to set the proper version (folder name).
To start the server run `npm run elasticsearch` in your command line. It will then run on `localhost:9200` by default. You can check this by opening `http://localhost:9200/?pretty`.

Here are some urls you can check:

* http://localhost:9200
* http://localhost:9200/?pretty
* http://localhost:9200/_cat/indices?v
* http://localhost:9200/_count
* http://localhost:9200/wf-test/_search?q=*&wiskunde
* http://localhost:9200/wf-test/_search?q=wiskunde

#### Populating the Elasticsearch server

To populate the Elasticsearch server with oData simply run `npm run elasticsearch:import`.
This might take a while (ie ten minutes).

#### Elasticsearch monitors and data browsers

Numerous Elasticsearch apps exist to give your server a GUI:

 - [Sense](https://chrome.google.com/webstore/detail/sense-beta/lhjgkmllcaadmopgmanpapmpjgmfcfig)
 - [ElasticSearch Toolbox](https://chrome.google.com/webstore/detail/elasticsearch-toolbox/focdbmjgdonlpdknobfghplhmafpgfbp)
 - [Dejavu](https://chrome.google.com/webstore/detail/dejavu/lcanobbdndljimodckphgdmllahfcadd)
 - [Elastichq](http://www.elastichq.org/)
 - [Marvel](https://www.elastic.co/products/marvel)
 - [Kibana](https://www.elastic.co/products/kibana)

#### CURL

For hardcore manipulation you can use CURL commands like so:

```
curl -XDELETE "http://localhost:9200/hodex"
```

```
curl -XPOST "http://localhost:9200/hodex/_search?pretty" -d "{
  "query": { "match_all": {} }
}"
```

```
curl -XPOST "http://localhost:9200/hodex/_search?pretty" -d "{
  "query": { "match": { "programDescriptions.programDescription.**": "bio*" } }
}"
```

```
curl -XGET "http://localhost:9200/hodex/_search" -d "{
    'query' : {
        'term' : { 'nl' : 'biology' }
    }
}"
```