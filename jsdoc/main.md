

This is a widget with which you can search.

# Generic implementation

To get the widget up and running on your site the only thing you have to do is include [the Javascript file](scripts/sk123ow.min.js) and call the widget. The following code would go at the end of your `body`:

```html
<!-- a container -->
<div id="example"></div>
<!-- loading the script -->
<script src="sk123ow.min.js"></script>
<!-- initialising the widget -->
<script>
    sk123ow(document.getElementById('example'),{key:'whateverYourKeyIs'});
</script>
```

And result in:

<div id="example"></div>

## The Javascript call

The first parameter in the Javascript call is the target for your widget so it must be of the type HTMLElement.<br/>
The second parameter is an options object which can be used to change the look-and-feel or the functionality of the widget.

```javascript
sk123ow(
    document.getElementById('example') // HTMLElement
    ,{key:'whateverYourKeyIs'} // options
);
```

## Promise

Since the initialisation of the widget is asynchronous the widget implements a [Promise](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise), a design pattern often used when dealing with asynchronous code. This means you can chain your call like this:

```javascript
sk123ow(document.getElementById('example'),{key:'whateverYourKeyIs'})
    .then(
    	// 'resolve' Function parameter fires when initialisation is successfully completed
			function(){
					console.log('The widget has initialised.');
			}
			// 'reject' Function parameter only fires when an error occurs during initialisation
			,function(err){
					console.log('Something bad happened, see:',err);
			}
		)
;
```

## Options

Options can be set with the second parameter. The minimum amount of options you should set are `key` and `host`. The key is a hash that verifies the domain your widget runs on. The host is the server Elasticsearch is running on (it defaults to localhost).

Here you the options object used to set the result table header to 'name' and 'location'.

```javascript
sk123ow(
    document.getElementById('example')
    ,{
        key:'whateverYourKeyIs'
        ,thead: ['name','location']
    }
);
```

The following options can be set. Please note the default behaviour, for instance: it's of no use to set {lang:'nl'} since that is the default setting.

{{options}}
