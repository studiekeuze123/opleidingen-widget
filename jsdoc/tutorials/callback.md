As explained in [the options](./#option-resultClick) you have the ability to add a callback method for when a specific search result is clicked.<br/>
This can be useful for when you want to override the default behaviour. Normally the additional data will fold out from under the search result. But you might want this to appear somewhere else; in a different element, a lightbox or maybe even a different page.<br/>

The default behaviour of showing the details in the table can be overridden by either setting the [option.details](./#option-details) to and empty Array, or by setting the [option.detailsTarget](./#option-detailsTarget).<br/>
If you set the option.detailsTarget -and- set the option.details to an empty Array you will have to populate the detailsTarget yourself.

The following code sets the resultClick callback function and the detailsTarget to show the details in a [modal box](http://getbootstrap.com/2.3.2/javascript.html#modals). The commented code shows how to reveal your own details instead of the default definition list.

```html
<div id="example"></div>
<!-- modal is directly copied from the modal example on getbootstrap.com -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title text-center" id="myModalLabel">Modal title</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
```

```javascript
// cache our variables
var mModal = document.getElementById('myModal')
    ,mModalTitle = mModal.querySelector('.modal-title')
    ,mModalContent = mModal.querySelector('.modal-body')
    ,$Modal = $('#myModal') // we need the jQuery-object for the Bootstrap modal
;

// initialise the widget
sk123ow(
    document.getElementById('example')
    ,{
        host: 'insert server'
        ,key: 'insert key'
        ,resultClick: showModal // the callback function
		,detailsTarget: mModalContent // the HTMLElement to show our details in
        //,details: []
    }
);

function showModal(data){
    console.log('showModal',arguments); // log the callback arguments
    mModalTitle.textContent = data.programDescriptions.programName.nl; // use callback data to set the title of the modal window
    //mModalContent.textContent = data.programDescriptions.programDescription.nl;
    $Modal.modal(); // show the modal
}
```

<div id="example"></div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title text-center" id="myModalLabel">Modal title</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>