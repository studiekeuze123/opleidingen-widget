# Bootstrap

The widget uses [Twitter Bootstrap](http://getbootstrap.com/) (only the CSS, not the Javascript).<br/>
When loaded it will first check whether Bootstrap was already added, if not: the minified CSS is loaded from CDN.

The look and feel can be adjusted by either overriding in your own stylesheets, or by adding a Bootstrap theme.

For instance the widget below is styled with [the Cosmo theme](//bootswatch.com/cosmo/) from [Bootswatch](//bootswatch.com), set by through the 'CDNCSS' option with one of [these](http://www.bootstrapcdn.com/#bootswatch_tab). Additionally the background of the widget is set through an inline style.

```html
<div id="example"></div>
<style>
#sk123ow {
    background-image:none; /* the default is a gradient */
    background-color:#FC2;
}
</style>
```

```javascript
sk123ow(document.getElementById('example'),{
    host: 'insert server'
    ,key: 'insert key'
    ,numResults: 5
    ,CDNCSS:{theme:'//maxcdn.bootstrapcdn.com/bootswatch/3.3.0/yeti/bootstrap.min.css'}
});
```

<div id="example"></div>
<style>
#sk123ow {
    background-image:none;
    background-color:#FC2;
}
</style>
