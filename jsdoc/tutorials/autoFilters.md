# AutoFilters

Default search results can be overridden using autoFilters.
The option autoFilter is an object with key-value pairs that will serve as the default filters. If a key is used that is not in the filters it will act as a read-only filter, if the key is present in the filters it's value will serve as the 'selected' value of the filter dropdown.

Below a combination of both is shown: the orgUnitId that is searched for is always 'hku'. The initial location is 'Hilversum' but this can be changed by using the filters.

```javascript
sk123ow(document.getElementById('example'),{
    host: 'insert server'
    ,key: 'insert key'
	,autoFilters: {
		"programClassification.orgUnitId": 'hku'
		,"programClassification.orgUnitLocation": 'Hilversum'
	}
	,filters: ['programClassification.orgUnitLocation']
});
```

<div id="example"></div>