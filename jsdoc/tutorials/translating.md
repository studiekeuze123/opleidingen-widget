# Translating the widget

Translating the widget can be done in two ways: one is by creating translation files that can be rendered to source code. The other (easier) way is by passing translations in the initialisation object.
The first should be used if you want to translate the entire widget to any other language than English or Dutch (which are already included in the source). The latter should only be used if you want to override specific translations.

## Translation by creating translation files

A universally adopted way for localizing software is an implementation called [gettext](https://en.wikipedia.org/wiki/Gettext). This implementation uses base translation files (.pot) to create localized translation files (.po and .mo).
The build script for this widget has a command that converts the translation files to Javascript. This does require you to have full access to the source code and have installed the dependencies (see `/README.md`).

The workflow for adding a new language is as follows:

 * Use a program like [Poedit](https://poedit.net/) to load the base translation file located here: `/lang/sk123ow.pot`.
 * Create the translations for a specific iso code and save the file to the same directory as the base file. The resulting filename should look like this: `sk123ow-iso.po` (where 'iso' is a language iso code like 'nl' or 'en').
 * Run the grunt command `grunt lang` to render all .po files to .js.
 * Locate the translated file `/src/js/sk123ow.lang.[iso].js` and add it to your build script or HTML source to be included *after* the widget main Javascript file `sk123ow.min.js`.
 * In your initialisation call you can now pass the newly translated iso code in the initialisation object: `sk123ow(document.getElementById('example'),{lang:'iso'});`.


## Translation by using the initialisation object

The easier way to translate is to pass the translations directly into the initialisation object. This is the way to go if you want only a few things translated or if you want to override existing translations. The translatable keys can be found in `/lang/sk123ow.pot`.
Say you have a language filter and want to use words instead of the language iso codes, and you want to change the widget title. You would go about it this way:

<small>Generally you'd only have to translate one iso, but for this example I've included both 'en' and 'nl' translations. </small>

```javascript
sk123ow(document.getElementById('example'),{
    translations: {
		en:{
			en: 'English'
			,nl: 'Dutch'
			,pap: 'Papiamento'
			,de: 'German'
			,es: 'Spanish'
		}
		,nl: {
			en: 'Engels'
			,nl: 'Nederlands'
			,pap: 'Papiamento'
			,de: 'Duits'
			,es: 'Spaans'
		}
    }
});
```

<div id="example"></div>

