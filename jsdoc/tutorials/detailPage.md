The [result callback](./#option-resultClick) can also be used to show your results on a different page.<br/>
For this example we'll simply send the required data to the next page using JSON and GET.<br/>

```javascript
sk123ow(
    document.getElementById('example')
    ,{
        host: 'insert server'
        ,key: 'insert key'
        ,resultClick: showDetailPage
    }
);
function showDetailPage(data){
    var oSend = {
            title: data.programDescriptions.programName
            ,description: data.programDescriptions.programDescription
        }
        ,sSend = JSON.stringify(oSend);
    location.href = 'detailPage.html?data='+encodeURIComponent(sSend);
}
```

The details page contains a simple script to parse the data:

```javascript
(function($){
    var $ResultData = $('.resultData')
        ,oData = JSON.parse(decodeURIComponent(location.search.split(/=/).pop()));
    for (var s in oData) {
        $('<li><strong>'+s+'</strong>: '+JSON.stringify(oData[s])+'</li>').appendTo($ResultData);
    }
})(jQuery);
```

Note that using HTTP GET for sending variables is limited in terms of size. It is 'mostly' not possible to send the entire result object using GET. If you want that you'll have to resort to [sessionStorage](https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Storage#sessionStorage) or something similar.

<div id="example" />