/**
 * Jsdoc js copied and refactored a bit from Docstrap.
 * Could use a major overhaul.
 */
iddqd.ns('jsdoc',(function($,u){
	'use strict';

	var loadScript = iddqd.pattern.callbackToPromise(iddqd.loadScript)
		// ,sElasticServer = 'http://836bc52d81ba33df000.qbox.io'
		// ,sElasticServer = 'elasticsearch.kiesopmaat.nl'
		// ,sWidgetKey = '=NjY0OTM4MDc5LjIwNjI4MzA4NzMuMzQ4NDkzNDMuMTc3ODY4OTk3LjE3Nzg2ODk5Ny41MjczMzY2OTg'
		,elasticServer = 'http://localhost:9200'
		,widgetKey = '=NTI3MzM2Njk4LjI2NDM2MTIzOTU1NjIxMDY3LjIwNzgyOS4zNzE5NzA3MTAyMDcxNy4uNTAwMzY3MzcyMQ='
		,index = 'map-test'
	;

	function init(options){
		options.collapseSymbols&&initCollapseSymbols();
		initTableOfContents();
		initPreCode();
		initHash();
		initTutorials();
	}

	function initCollapseSymbols(){
		$('#main').localScroll({
			offset: { top: 56 } //offset by the height of your header (give or take a few px, see what works for you)
		});
		$("dt h4.name").each(function () {
			var $this = $(this);
			var icon = $("<i/>").addClass("icon-plus-sign").addClass("pull-right").addClass("icon-white");
			var dt = $this.parents("dt");
			var children = dt.next("dd");

			$this.append(icon).css({cursor: "pointer"});
			$this.addClass("member-collapsed").addClass("member");

			children.hide();
			$this.toggle(function () {
				icon.addClass("icon-minus-sign").removeClass("icon-plus-sign").removeClass("icon-white");
				$this.addClass("member-open").removeClass("member-collapsed");
				children.slideDown();
			},function () {
				icon.addClass("icon-plus-sign").removeClass("icon-minus-sign").addClass("icon-white");
				$this.addClass("member-collapsed").removeClass("member-open");
				children.slideUp();
			});
		});
	}

	function initTableOfContents(){
		var sPath = location.pathname.split('/').pop()
			, isIndex = sPath==='' || sPath==='index.html';
		if (isIndex) {
			$("#toc").detach();
		} else {
			$("#toc").toc({
				selectors: "h1,h2,h3,h4,dt>a",
				showAndHide: false,
				scrollTo: 60
			});
			$("#toc>ul").addClass("nav nav-pills nav-stacked");
			$("#main span[id^='toc']").addClass("toc-shim");
			if ($("#toc>ul").html()==='') $("#toc").detach();
		}
	}

	function initPreCode(){
		Array.prototype.forEach.call(document.querySelectorAll('pre.source'),function(pre){
			var aMatchLang = pre.getAttribute('class').match(/lang-(\w+)/)
				,sLang = aMatchLang.length>1?aMatchLang[1]:u
				,mCode
			;
			if (sLang) {
				mCode = pre.querySelector('code');
				if (mCode&&!mCode.classList.contains('rainbow')) {
					mCode.setAttribute('data-language',sLang);
				} else if (mCode&&mCode.classList.contains('rainbow')) {
					console.warn('Rainbow already initialised for',mCode);
				}
			}
		});
	}

	function initHash(){
		if (location.hash) {
			setTimeout(function () {
				$(location.hash).addClass('highlight');
				$.scrollTo(location.hash,500,{axis: 'y',offset: -50});
			},500);
		}
	}

	function initTutorials(){
		console.log('initTutorials'); // log
		/*global jsdoc*/
		var aMatchTutorial = location.pathname.match(/\/tutorial-([^.]*).html/);
		if (aMatchTutorial) {
			var sTutorial = aMatchTutorial.pop();
			if (jsdoc.tutorial.hasOwnProperty(sTutorial)) {
				jsdoc.tutorial[sTutorial]();
			}
		} else { // presume frontpage
			loadScript('scripts/sk123ow.min.js').then(function (){
				sk123ow(
					document.getElementById('example')
					,{
						key: widgetKey
						,host: elasticServer
						,index: index
					}
				);
			});
		}
	}

	return {init:init,elasticServer:elasticServer,widgetKey:widgetKey,index:index};
})(jQuery));