/*global jsdoc*/
iddqd.ns('jsdoc.tutorial.autoFilters',(function(){
	'use strict';

	var loadScript = iddqd.pattern.callbackToPromise(iddqd.loadScript)
		,loop = iddqd.loop
	;

	function init(){
		return Promise.all((function(a){
			loop([
				'scripts/sk123ow.min.js'
			],function(uri){
				a.push(loadScript(uri));
			});
			return a;
		})([]))
			.then(initWidget);
	}

	function initWidget(){
		sk123ow(document.getElementById('example'),{
			host: jsdoc.elasticServer
			,key:jsdoc.widgetKey
			,index:jsdoc.index
			,autoFilters: {
				Financing: 'government'
				// ,'OrgUnitLocations.Location': 'Hilversum'
			}
			,filters: ['ProgramLevel']
			,showFilters: true
			,resultNum: 5
		});
	}

	return init;
})());