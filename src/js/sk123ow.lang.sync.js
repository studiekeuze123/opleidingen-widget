/**
 * Do not add to namespace!
 * Solely for po-edit auto-sync.
 * These are l18n keys that are not hard-coded but found through elasticsearch queries.
 */
(function(__) {
	'use strict';
	return function() {
		// Language ISO codes
		__('en');
		__('nl');
		__('de');
		__('es');
		__('fr');
		__('it');
		__('pap');
		//
		// Messages
		__('noResults');
		//
		// Error messages
		__('errorCSSLoad');
		__('errorScriptLoad');
		__('invalidKey');
		__('oneInstance');
		__('loadError');
		//
		// Aliases
		__('lang');
		//
		// programType
		__('regular');
		__('educational');
		__('post-initial');
		__('research');
		//
		// Elasticsearch 'program' hit (extended)
		__('ActiveProgram');
		__('City');
		__('CourseId');
		__('CourseLanguageStringId');
		__('CourseLanguageStrings');
		__('CourseObjectives');
		__('CrohoCode');
		__('CrohoVariant');
		__('Dual');
		__('Duration');
		__('ErasmusMundusProgram');
		__('Faculty');
		__('FieldOfEducation');
		__('Financing');
		__('FullTime');
		__('HasHonoursProgram');
		__('HodexID');
		__('Instelling');
		__('InstellingEngels');
		__('Instelling_SK123');
		__('Instelling_SK123ID');
		__('InstitutionId');
		__('InstitutionLanguageStringId');
		__('InstitutionLanguageStrings');
		__('Language');
		__('Location');
		__('NumerusFixus');
		__('OrgUnitDescription');
		__('OrgUnitId');
		__('OrgUnitLocationId');
		__('OrgUnitLocations');
		__('OrgUnitName');
		__('OrgUnitSummary');
		__('OrgUnitType');
		__('PartTime');
		__('PercentageDistanceLearning');
		__('PercentageForeignStudents');
		__('PercentageForeignTeachers');
		__('PercentageNightEducation');
		__('PercentageStudentsAbroad');
		__('PercentageTeachersAbroad');
		__('ProgramCredits');
		__('ProgramDescription');
		__('ProgramLevel');
		__('ProgramName');
		__('ProgramSummary');
		__('Qualification');
		__('SizeOfTutorialGroups');
		__('SourceUrl');
		__('StudyChoiceCheckDescription');
		__('StudyChoiceCheckName');
		__('StudyChoiceCheckPolicyOfAdmissionAfter1May');
		__('StudyChoiceCheckPolicyOfAdmissionBefore1May');
		__('StudyChoiceCheckRequired');
		__('StudyChoiceCheckStudyChoiceCheckWebLink');
		__('StudySubject');
		__('Weblink');
		__('YearlyStudyCosts');
	};
})(function(){});