/**
 * Studiekeuze123 Opleidingen Widget
 * @summary Studiekeuze123 Opleidingen Widget
 * @namespace sk123ow
 * @license none
 * @author Ron Valstar (http://ronvalstar.nl/)
 * @copyright Studiekeuze123
 * @todo unit testing
 * @todo check window.sk123ow.lang.* for redundant translations (ie [OrgUnitId]orgUnitDescription)
 * @todo store organisations in cookie
 * @todo a 401 on msearch does not prevent further initialisation, it should
 */
window.sk123ow = (function(undef) {
	'use strict';

	var l18n = iddqd.l18n
		,__ = l18n.__
		,addLang = l18n.add
		,type = iddqd.type
		,loop = iddqd.loop
		,extend = iddqd.extend
		,createElement = iddqd.createElement
		,pattern = iddqd.pattern
		,findParent = iddqd.internal.host.htmlelement.findParent
		,memoize = pattern.memoize
		,loadScript = pattern.callbackToPromise(iddqd.loadScript)
		,hash = murmurhash3_32_gc
		,promise = function(c){return new Promise(c);}
		,documentCreateElement = document.createElement.bind(document)
		,createDocumentFragment = document.createDocumentFragment.bind(document)
		,getElementById = document.getElementById.bind(document)
		//
		,version = '2.0.14'
		,isInitialised = false
		//
		,self = location.host
		,host = 'localhost:9200'
		,elasticIndex = 'wf-test'
		//
		,doUseCSS = true
		,widgetLangISO = 'nl'
		//
		/*jshint -W044 */
		,html = '<div id="sk123ow" class="sk123ow"><h3>Opleidingen widget</h3><form role="form"><div class="input-group input-group-lg"><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span> <input id="search" type="text" class="form-control" placeholder="query"> <span class="input-group-btn"><button class="btn btn-default" type="submit">Search</button></span></div><div class="reveal panel panel-sm clearfix"><label for="revealFilters" class="btn btn-sm"><span class="glyphicon glyphicon-filter"></span></label><input id="revealFilters" class="hide" type="checkbox"><div class="form-inline"><div class="form-group form-group-sm"><div class="input-group"><label for="selectTaal">Taal</label><select id="selectTaal" class="form-control" multiple=""><option value="" default="">taal</select></div></div></div></div></form><div class="alert alert-warning hide" role="alert"><span class="glyphicon glyphicon-warning-sign"></span><span class="text">Your search yielded no results.</span></div><div id="searchResult"><div class="panel panel-default"><div class="panel-heading"></div><div class="table-responsive"><table class="table table-condensed table-striped table-hover"><thead><tr><tbody></table></div><nav class="pages"><ul class="pagination pagination-sm"></ul></nav></div></div><footer class="text-center"><small>sk123ow V <span></span></small></footer></div>'
		,css = '.sk123ow{box-sizing:border-box;padding:10px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;background-color:rgba(7,104,159,0.91);background-repeat:repeat-x;background-image:-khtml-gradient(linear, left top, left bottom, from(#087ab8), to(rgba(7,104,159,0.91)));background-image:-moz-linear-gradient(#087ab8, rgba(7,104,159,0.91));background-image:-ms-linear-gradient(#087ab8, rgba(7,104,159,0.91));background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0, #087ab8), color-stop(100%, rgba(7,104,159,0.91)));background-image:-webkit-linear-gradient(#087ab8, rgba(7,104,159,0.91));background-image:-o-linear-gradient(#087ab8, rgba(7,104,159,0.91));filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#087ab8\', endColorstr=\'rgba(7, 104, 159, 0.91)\', GradientType=0);-ms-filter:"progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#087ab8\', endColorstr=\'rgba(7, 104, 159, 0.91)\', GradientType=0)";background-image:linear-gradient(#087ab8, rgba(7,104,159,0.91))}.sk123ow .clearfix{zoom:1}.sk123ow .clearfix:before,.sk123ow .clearfix:after{content:\'\';display:table}.sk123ow .clearfix:after{clear:both}.sk123ow *,.sk123ow *:before,.sk123ow *:after{box-sizing:inherit}.sk123ow h3{margin:0;font:700 20px/20px Arial,sans;color:#FFF}.sk123ow form{margin-top:10px}.sk123ow form .reveal{position:relative;z-index:1;margin-top:-1px;padding-top:1px;border-top-left-radius:0;border-top-right-radius:0;border-top-color:#EEE;display:inline-block;background-color:#eee}.sk123ow form label[for=revealFilters]{width:50px}.sk123ow form label[for=revealFilters],.sk123ow form .form-inline{float:left}.sk123ow form .form-inline .form-group{vertical-align:top}.sk123ow form #revealFilters+.form-inline{display:none}.sk123ow form #revealFilters:checked+.form-inline{display:block}.sk123ow.hasfilters form .input-group-lg .input-group-addon{border-bottom-left-radius:0}.sk123ow select{text-transform:capitalize}.sk123ow select[multiple]{height:140px}.sk123ow option:checked{background-color:#EEE}.sk123ow option[default]:before{content:\'...\'}.sk123ow #searchResult{max-height:0;-moz-transition:max-height 300ms ease-out;-ms-transition:max-height 300ms ease-out;-webkit-transition:max-height 300ms ease-out;-o-transition:max-height 300ms ease-out;transition:max-height 300ms ease-out;overflow:hidden}.sk123ow #searchResult.reveal{max-height:9999px}.sk123ow #searchResult thead th{white-space:nowrap;text-transform:capitalize}.sk123ow #searchResult thead th.sort:after{content:\'\e114\';display:inline-block;padding-left:5px;font-family:\'Glyphicons Halflings\';font-style:normal;font-weight:normal;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;font-size:10px;line-height:100%}.sk123ow #searchResult thead th.sort.desc:after{content:\'\e113\'}.sk123ow #searchResult.searching tbody{opacity:.5}.sk123ow #searchResult .info td[colspan]{display:table-cell}.sk123ow #searchResult .info.all-details td>dl{zoom:.7}.sk123ow #searchResult .info.info-details td>*{max-height:1px;-moz-transition:max-height 300ms ease-out;-ms-transition:max-height 300ms ease-out;-webkit-transition:max-height 300ms ease-out;-o-transition:max-height 300ms ease-out;transition:max-height 300ms ease-out;overflow:hidden}.sk123ow #searchResult .info.reveal td>*{max-height:500px}.sk123ow #searchResult .info dl,.sk123ow #searchResult .info dt,.sk123ow #searchResult .info dd{vertical-align:top}.sk123ow #searchResult .info dt{text-transform:capitalize}.sk123ow #searchResult .info dd:before{content:\'\';position:absolute;left:0;top:0;width:100%;height:1px;background-color:rgba(255,255,255,0.5)}.sk123ow #searchResult .info dd a{display:inline-block;max-width:75%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.sk123ow .pages{overflow:hidden}.sk123ow .pagination{width:100%;text-align:center}.sk123ow .pagination li{display:inline-block}.sk123ow .pagination .edge-left a{border-right:1px solid #087ab8;z-index:1}.sk123ow .pagination .edge-right a{border-left-color:#087ab8}.sk123ow .pagination a{width:30px;height:30px;padding:0;line-height:30px;text-align:center}.sk123ow .alert .text{padding-left:10px}.sk123ow footer{color:rgba(255,255,255,0.5)}'
		/*jshint +W044 */
		//
		,CDNCSS = {
			bootstrap: '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css'
		}
		,CDNJS = {
			es5: '//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js'
			,elasticsearch: '//cdnjs.cloudflare.com/ajax/libs/elasticsearch/3.0.0/elasticsearch.min.js'
		}
		//
		// Elasticsearch variables
		,elasticsearchClient
		,searchObj
		,search
		//
		// HTMLElements
		,elmWidget
		,elmSearchForm
		,elmSearch
		,elmReveal
		,elmFilterClone
		,elmResultTarget
		,elmDetailsTarget
		,elmAlert
		//
		,page = 0
		,perPage = 10
		//
		,doStartWithResult = true
		,doShowFilters = false
		,hasShortcuts = false
		//
		// searchFields
		,searchType = 'program' // organisation
		,searchFields = [
			'CourseLanguageStrings.**'
			,'InstitutionLanguageStrings.**'
			,'OrgUnitLocations.**'
		]
		//
		// filters
		,filters = {}
		,filtersAuto = {}
		,filterList = [
			'ProgramLevel'
			,'InstructionLanguages.LanguageCode'
			,'OrgUnitLocations.Location'
		]
		,filtersRaw = [
			// 'ProgramType'
			// ,'ProgramLevel'
			// ,'OrgUnitLocations.Location'
		]
		,languageISOS = ['en','nl','pap','de','es']
		//
		// search result
		,elmSearchResult
		,elmSearchResultHeading
		,elmTable
		,elmThead
		,elmTableBody
		,elmPagination
		,searchResult
		,columns = [
			'ProgramName'
			,'OrgUnitName'
			,'ProgramLevel'
			,'Location'
		]
		,columnsNum = columns.length
		,isFirstResult = false
		/**
		 * This is the callback invoked when the options.resultClick key is set on the init options and a TR element in the search result is clicked.
		 * @callback sk123ow~resultClickCallback
		 * @param {object} result The result object associated with the clicked element.
		 * @param {HTMLElement} element The element that was clicked
		 */
		,fnBodyClickCallback
		//
		// details
		,hasAllDetails = false
		,details = [
			'ProgramDescription'
			,'ProgramCredits'
			,'OrgUnitDescription'
			,'Location'
			,'ProgramLevel'
			,'Weblink'
		]
		//
		// classNames
		,classNameHide = 'hide'
		,classNameSearching = 'searching'
		,classNameSort = 'sort'
		,classNameDesc = 'desc'
		,classNameAsc = 'asc'
		//
		,key = {
			LEFT: 37
		  ,RIGHT: 39
		  ,HOME: 36
		  ,END: 35
		}
		// error
		,error
		//
		,organisations = {}
		,mappings
		,mappingsDeep = {}
	;

	/**
	 * Main initialisation function.
	 * Methods are chained because some are asynchronous.
	 * @param {HTMLElement} target The target HTMLElement
	 * @param {object} [options] An optional object with options
	 * @param {string} [options.host='localhost:9200'] The Elasticsearch host uri.<br/> This option corresponds to that of {@link http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/configuration.html#config-hosts|Elasticsearch JS-API config host}.
	 * @param {string} [options.index='wf-test'] The Elasticsearch index name. Also see {@link http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/_add_an_index.html|Elasticsearch: Adding and index}.
	 * @param {string} [options.lang='nl'] The ISO language code for the widget language. Setting this option also to anything other than 'nl' or 'en' requires you to add a language file: 'sk123ow.lang.[iso].js' (easily added by adding the location to option.CDNJS). Note that this option has nothing to do with the search result language.
	 * @param {string} [options.translations=sk123ow.lang] An object with translations by ISO code. IE: `{en:{cookie:'Cookie'},nl:{cookie:'Koekje'}}`. Contrary to other options this one will extend the default, so setting one term will not delete the others.
	 * @param {string[]} [options.filters=['programLevel','programType','instructionLanguage','location']] Sets the filter dropdowns
	 * @param {object} [options.autoFilters={}] An object with key-value pairs that will serve as the default filters. If a key is used that is not in the filters it will act as a read-only filter, if the key is present in the filters it's value will serve as the 'selected' value of the filter dropdown.
	 * @param {number} [options.resultNum=10] The number of results per page.
	 * @param {string[]} [options.resultCols=['name','orgUnitId','location','type','level']] The search result columns
	 * @param {HTMLElement} [options.resultTarget] An HTMLElement to render the search-result in.
	 * @param {sk123ow~resultClickCallback} [options.resultClick] A callback function for when a search result is clicked. The callback has two parameters. The first is an object with the result data, the second is the clicked HTMLElement. Also see the {@tutorial callback} tutorial.
	 * @param {string[]} [options.details=['programDescription','programCredits','orgUnitDescription','orgUnitLocation','programLevel','webLink']] The details to show when a result is clicked. An empty array will not show details.
	 * @param {HTMLElement} [options.detailsTarget] An HTMLElement to render the details-result in. Details are always rendered as a definition list (DL, DT, DD).
	 * @param {{es5:Number,elasticsearch:string}} [options.CDNJS={es5:'//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js',elasticsearch:'//cdnjs.cloudflare.com/ajax/libs/elasticsearch/3.0.0/elasticsearch.min.js'}] Object containing CDN Javascript uris. These could also be relative paths.
	 * @param {{bootstrap:string}} [options.CDNCSS={bootstrap:'//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css'}] Object containing CDN CSS uris. These could also be relative paths. Also see the {@tutorial styling} tutorial.
	 * @param {boolean} [options.disableCSS=false] Disable dynamic adding of stylesheets. When you set this to true you will have to style everything yourself.
	 * @param {boolean} [options.startWithResult=true] Disable dynamic adding of stylesheets. When you set this to true you will have to style everything yourself.
	 * @param {boolean} [options.showFilters=false] Show filters by default
	 * @param {boolean} [options.shortcuts=false] Keyboard shortcuts
	 * @param {string} [options.searchType='program'] The object type to search for. Changing this also migth require adjusting the options: searchFields, resultCols and details.
	 * @param {string[]} [options.searchFields=[...]] The fields to search within the searchType.
	 * @param {string} [options.key] The widget key (hash).
	 * @returns {Promise}
	 * @memberof sk123ow
	 * @method
	 * @public
	 */
	function init(target,options){
		error = sk123ow.error;

		return promise(function(resolve,reject){
			if (isInitialised) {
				reject(error.ONE_INSTANCE);
			} else {
				isInitialised = true;
				resolve();
			}
		})
			.then(initHash.bind(undef,options.key))
			.then(initOptions.bind(undef,options))
			.then(initCSS)
			.then(initJS) // so no es5 or elasticsearch above
			.then(initLang) // after initJS because additional lang files could be loaded
			.then(initElasticsearch)
			.then(initDOM.bind(undef,target))
			.then(initEvents)
			.then(initView)
			.then(initCall)
			.then(initFilters)
			.then(initStart)
		;
	}

	/**
	 * Verifies the key to the current hostname
	 * @param {string} key
	 * @returns {Promise}
	 */
	function initHash(key){
		return promise(function(resolve,reject){
			var lastTwo = (function(a){
					if (a.length>2) a.length = 2;
					return a.reverse().join('.');
				})(self.split('.').reverse()).replace(/:\d+/,'')
				,hashed = hash(lastTwo,lastTwo.length)
				,swappedKey = key.substr(1)+key[0]
				,isValid = false
				,decoded
			;
			try {
				decoded = atob(swappedKey);
			} catch(err){}
			decoded&&decoded.split('.').forEach(function(id){
				if (!isValid&&hashed===parseInt(id,10)) isValid = true;
			});
			if (isValid) {
				resolve();
			} else {
				reject(error.INVALID_KEY);
				error(error.INVALID_KEY);
			}
		});
	}

	/**
	 * Process any parsed initialisation options. @see sk123ow.init
	 * @param options
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initOptions(options){
		loop(options,function(value,option){
			switch (option) {
				case 'host': host = value; break;
				case 'index': elasticIndex = value; break;
				//
				case 'lang': widgetLangISO = value; break;
				case 'translations':
					loop(value,function(dict,iso){
						extend(sk123ow.lang[iso],dict,true);
					});
				break;
				case 'filters': filterList = value; break;
				case 'autoFilters': filtersAuto = extend(value,filtersAuto); break;
				case 'resultNum': perPage = value; break;
				case 'resultCols':
					columns = value;
					columnsNum = columns.length;
				break;
				case 'resultTarget': elmResultTarget = value; break;
				case 'resultClick': fnBodyClickCallback = value; break;
				case 'details': details = value; break;
				case 'detailsTarget': elmDetailsTarget = value; break;
				case 'CDNJS': CDNJS = extend(value,CDNJS); break;
				case 'CDNCSS': CDNCSS = extend(value,CDNCSS); break;
				case 'disableCSS': doUseCSS = !value; break;
				case 'startWithResult': doStartWithResult = value; break;
				case 'showFilters': doShowFilters = value; break;
				case 'shortcuts': hasShortcuts = value; break;
				case 'searchType': searchType = value; break;
				case 'searchFields': searchFields = value; break;
			}
		});
	}

	/**
	 * Initialise l18n
	 */
	function initLang(){
		l18n.init(widgetLangISO,sk123ow.lang);
	}

	/**
	 * Initialise stylesheets. Searches document.styleSheets for bootstrap.css and adds it if missing.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initCSS(){
		if (doUseCSS) {
			var elmHead = document.querySelector('head')
				,elmFirstCSS = elmHead.children[0]
				,elmStyle = createElement('style')
				,hasBootstrap = false
				,searchFor = '.glyphicon-asterisk:'
			;
			loop(document.styleSheets,function(styleSheet){
				// IE and Firefox can throw SecurityError that block instantiation, might be nice to fix without try/catch
				try{
					loop(styleSheet.cssRules,function(styleRule){
						var cssText = styleRule.cssText;
						if (!hasBootstrap&&cssText&&cssText.indexOf(searchFor)!==-1) {
							hasBootstrap = true;
						}
					});
				}catch(err){console.warn(err);}
			});
			if (!hasBootstrap) {
				loadCSS(CDNCSS.bootstrap,elmHead,elmFirstCSS).catch(error);
			}
			loop(CDNCSS,function(uri,id){
				if (id!=='bootstrap') {
					loadCSS(uri,elmHead,elmFirstCSS).catch(error);
				}
			});
			elmStyle.innerHTML = css;
			elmHead.insertBefore(elmStyle,elmFirstCSS);
		}
	}

	/**
	 * Load Js from CDN
	 * @returns {Promise}
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initJS(){
		return Promise.all((function(a){
			loop(CDNJS,function(uri){
				a.push(loadScript(uri));
			});
			return a;
		})([]));
	}

	/**
	 * Initialise Elasticsearch, memoize the search method to prevent duplicate server calls.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initElasticsearch(){
		elasticsearchClient = new elasticsearch.Client({ host: host });
		search = memoize(function(o,handleResult){
			elasticsearchClient.search(o).then(handleResult,error);
		},null,true);
	}

	/**
	 * Create and add HTML to DOM.
	 * Initialise all DOM related variables
	 * @param target
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initDOM(target){
		var elmWrap = documentCreateElement('div')
			,title = __('Opleidingen widget')
			,elmTitle;
		elmWrap.innerHTML = html;
		while (elmWrap.children.length) {
			target.appendChild(elmWrap.children[elmWrap.children.length-1]);
		}
		//
		elmWidget = getElementById('sk123ow');
		//
		elmTitle = elmWidget.querySelector('h3');
		if (title!=='') elmTitle.textContent = title;
		else elmTitle.parentNode.removeChild(elmTitle);
		//
		elmSearchForm = elmWidget.querySelector('form');
		elmSearch = getElementById('search');
		elmSearch.setAttribute('placeholder',__('query'));
		elmSearchForm.querySelector('button[type=submit]').textContent = __('Search');
		//
		elmReveal = elmSearchForm.querySelector('.reveal');
		if (filterList.length===0) {
			elmReveal.parentNode.removeChild(elmReveal);
		} else {
			elmWidget.classList.add('hasfilters');
			if (doShowFilters) getElementById('revealFilters').checked = true;
		}
		elmFilterClone = pickClone(elmReveal,'.form-group');
		//
		elmSearchResult = getElementById('searchResult');
		elmSearchResultHeading = elmSearchResult.querySelector('.panel-heading');
		setSearchResultHeading();
		elmTable = elmSearchResult.querySelector('table');
		elmThead = elmTable.querySelector('thead');
		elmTableBody = elmTable.querySelector('tbody');
		elmPagination = elmSearchResult.querySelector('.pagination');
		if (elmResultTarget) {
			createElement(undef,'sk123ow',elmResultTarget)
				.appendChild(elmSearchResult);
		}
		//
		elmAlert = elmWidget.querySelector('.alert');
		//
		elmWidget.querySelector('footer span').innerText = version;
	}

	/**
	 * Initialise event listeners.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initEvents(){
		elmSearchForm.addEventListener('change',handleFormChange);
		elmSearchForm.addEventListener('submit',handleSearchSubmit);
		elmThead.addEventListener('click',handleTheadClick);
		elmTableBody.addEventListener('click',handleTableClick);
		elmPagination.addEventListener('click',handlePaginationClick);
		window.addEventListener('resize',handleResize);
		hasShortcuts&&document.addEventListener('keyup',handleKeyUp);
	}

	/**
	 * Combines two initial queries to a single call.
	 *  - A query to return all the organisations because Elasticsearch does not do joined queries.
	 *  - Query to return all possible language ISO codes
	 * @returns {Promise}
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initCall(){
		var indexObject = {index: elasticIndex};
		return Promise.all([
			elasticsearchClient.indices.getFieldMapping({
				index:elasticIndex
				,field:'*'
			})
			,elasticsearchClient.msearch({
				body: [
					indexObject,{
						query:{bool:{must:[{match:{_type:'organisation'}}]}}
						,from: 0
						,size: 999
					}
				]
			})
		]).then(function(results){
			var resultMapping = results[0]
				,/**esResultM*/resultMSearch = results[1];
			handleMappingResult(resultMapping);
			handleOrganisationsResult(resultMSearch.responses[0]);
		},error);
	}

	/**
	 * Initialises an API aggregation call to index filter terms
	 * @returns {Promise}
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initFilters(){
		var searchBody = {
				aggs: filters
				,size: 0
			}
			,autoFiltersNotInFilters = {}
			,hasAutoFiltersNotInFilters = false
		;
		//
		filterList.forEach(function(filter){
			var nested = undef;
			// test for nested filter, ie: Location => OrgUnitLocations.Location
			loop(mappingsDeep,function(map){
				loop(map,function(key,base){
					if ((new RegExp('\\\.'+filter+'$')).test(key)) {
						nested = base;
						filter = key;
					}
				});
			});
			if (!nested&&filter.indexOf('.')!==-1) nested = filter.split('.').shift();
			if (nested===undef) {
				var field = filter + (filtersRaw.indexOf(filter)===-1?'':'.raw');
				filters[filter] = {
					terms: { field: field, size: 0 }
				};
			} else {
				var aggs = {};
				aggs[filter.split('.').pop()] = {
							terms: {
								field: filter
								,size: 0
							}
						};
				filters[filter] = {
					nested: {
						path: nested
					}
					,aggs: aggs
				};
			}
		});
		//
		loop(filtersAuto,function(val,key){
			if (filterList.indexOf(key)===-1) {
				autoFiltersNotInFilters[key] = val;
				hasAutoFiltersNotInFilters = true;
			}
		});
		if (hasAutoFiltersNotInFilters) {
			var autoMust = [];
			loop(autoFiltersNotInFilters,function(val,key){
				autoMust.push(getMatch(key,val));
			});
			searchBody.query = {
				filtered: {
				   query: {
						 bool: {
							 must: autoMust
						 }
					 }
				}
				/*filtered: {
				   query: { match_all: {} },
				   filter: { term: autoFiltersNotInFilters }
				}*/
			};
		}
		return elasticsearchClient.search({
			index: elasticIndex
			,body: searchBody
		}).then(handleFiltersResult,error);
	}

	/**
	 * Optionally initialise widget with default search result
	 * @returns {String} the Elasticsearch index that was initialised
	 */
	function initStart(){
		doStartWithResult&&handleSearchSubmit();
		return elasticIndex;
	}

	/**
	 * Initialise the view after Javascripts have loaded.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initView(){
		initTableHead();
	}

	/**
	 * Initialise the table head with th-elements
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function initTableHead(){
		var elmFragment = createDocumentFragment();
		columns.forEach(function(property){
			createElement('th',undef,elmFragment,{'data-key':property},__(property.split('.').pop()));
		});
		elmThead.querySelector('tr').appendChild(elmFragment);
	}

	/**
	 * Handles the API callback for the aggregation
	 * @param {esResult} resp
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function handleFiltersResult(resp){
		var responseAggregations = resp.aggregations
			,elmFilterWrap = elmWidget.querySelector('form .reveal div');
		loop(responseAggregations,function(responseAggregation,aggregation){
			var splitAggr = aggregation.split('.') // check nested
				,buckets = responseAggregation.buckets||responseAggregation[splitAggr.pop()].buckets;
			if (aggregation==='lang') { // fake buckets for lang
				languageISOS.forEach(function(iso){
					buckets.push({key:iso});
				});
			}
			var id = 'filter-'+aggregation
				,filterAggregation = filters[aggregation]
				,elmFilter = elmFilterClone.cloneNode(true)
				,elmLabel = elmFilter.querySelector('label')
				,elmSelect = elmFilter.querySelector('select')
				,elmDefaultOption = elmFilter.querySelector('option[default]')
				,isAutoFilter = filtersAuto.hasOwnProperty(aggregation)
				,autoFilter = isAutoFilter&&filtersAuto[aggregation]
			;
			elmLabel.textContent = __(aggregation.split('.').pop());
			elmLabel.setAttribute('for',id);
			elmSelect.setAttribute('id',id);
			if (elmSelect.getAttribute('multiple')) {
				elmSelect.removeChild(elmDefaultOption);
			} else {
				elmDefaultOption.textContent = __('noSelection');
			}
			buckets.forEach(function(bucket){
				var bucketKey = bucket.key;
				if (bucketKey.toLowerCase()!=='noselection') {
					var elmOption = createElement('option',null,elmSelect);
					elmOption.setAttribute('value',bucketKey);
					elmOption.textContent = __(bucketKey);
					if (isAutoFilter&&autoFilter.toLowerCase()===bucketKey.toLowerCase()) {
						elmOption.setAttribute('selected','1');
					}
				}
			});
			elmFilterWrap.appendChild(elmFilter);
			filterAggregation.element = elmSelect;
		});
	}

	/**
	 * Check the mapping of this index for nested keys
	 * @param {object} mapping
	 */
	function handleMappingResult(mapping){
		mappings = mapping[elasticIndex].mappings;
		loop(mappings,function(properties,type){
			var typeObj = mappingsDeep[type] = mappingsDeep[type]||{};
			loop(properties,function(property,key){
				var keySplit = key.split('.')
					,keyBase = keySplit.shift()
					,keyEnd = keySplit.pop();
				keyEnd&&!typeObj.hasOwnProperty(keyBase)&&(typeObj[keyBase] = key);
			});
		});
	}

	/**
	 * Handles the API callback for organisations
	 * @param {esResult} resp
	 */
	function handleOrganisationsResult(resp){
		loop(resp.hits.hits,function(organisation){
			var source = organisation._source
				,orgUnitId = source.OrgUnitId
				,langSource = source.InstitutionLanguageStrings
			;
			if (orgUnitId!==undef) {
				languageMapping(langSource,orgUnitId,'OrgUnitName');
				languageMapping(langSource,orgUnitId+'orgUnitDescription','OrgUnitDescription');
			}
		});
		loop(resp.hits.hits,function(/**esHit*/hit){
			var source = hit._source
					,sourceType = hit._type;
			organisations[source.OrgUnitId] = source;
			extendIso(source,'InstitutionLanguageStrings',widgetLangISO);
			extendExpands(source,mappingsDeep,sourceType);
		});
	}

	/**
	 * Handles form change. Checks if the target is a filter and subsequently submits the query
	 * @param {Event} e
	 */
	function handleFormChange(e){
		if (!!findParent(e.target,'div','form-inline')) handleSearchSubmit();
	}

	/**
	 * Handles the search form submit event. Sends out an API call for the search.
	 * @param {Event} [e]
	 * @memberof sk123ow
	 * @method
	 * @private
	 * @see http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/combining-filters.html#_nesting_boolean_filters
	 */
	function handleSearchSubmit(e){
		e&&e.preventDefault();
		var searchString = elmSearch.value.toString()
			,isSearch = searchString!==''
			,query = {
				bool: {
					must: [
						{match:{_type:searchType}}
					]
					,should: []
					,must_not: []
				}
			}
		;
		// search query
		if (isSearch) {
			query.bool.must.push({query_string: {
				fields: searchFields
				,query: searchString
			}});
		}
		//
		// autoFilter
		loop(filtersAuto,function(value,autoFilter){
			if (filterList.indexOf(autoFilter)===-1) {
				var match = {};
				match[autoFilter] = value;
				query.bool.must.push(getMatch(autoFilter,value));
			}
		});
		//
		// filters
		loop(filters,function(aggregation,field){
			var elmSelect = aggregation.element
				,elmsOptions = elmSelect.childNodes
				,value = elmSelect&&elmSelect.value.toString()
				,nestBool;
			if (value!=='') {
				var selected = [];
				for (var i=0,l=elmsOptions.length;i<l;i++) {
					var elmOption = elmsOptions[i];
					elmOption.selected&&selected.push(elmOption.value);
				}
				if (selected.length>1) {
					nestBool = {bool:{minimum_should_match: 1,should:[]}};
					selected.forEach(function(value){
						nestBool.bool.should.push(getMatch(field,value));
					});
					query.bool.should.push(nestBool);
					query.bool.minimum_should_match = 1;
				} else {
					query.bool.must.push(getMatch(field,value,query));
				}
			}
		});
		//
		page = 0;
		searchObj = {
			index: elasticIndex,
			body: {
				query: query
				,from: page
				,size: perPage
			}
		};
		doSearch(true);
	}

	/**
	 * Handles the API callback for a search.
	 * Shows the result in a table.
	 * @param {esResult} resp
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function handleSearchResult(resp){
		clearSearch();
		elmSearchResult.classList.remove(classNameSearching);
		searchResult = resp;
		//
		elmSearchResult.classList.add('reveal');
		setSearchResultHeading();
		//
		if (resp.hits.total===0) {
			showAlert(__('noResults'));
		} else {
			// flatten current lang to hit._source
			loop(resp.hits.hits,function(hit){
				var source = hit._source
					,sourceType = hit._type;
				console.log('\t',[extend({},source),source]);
				extendIso(source,'CourseLanguageStrings',widgetLangISO);
				extendExpands(source,mappingsDeep,sourceType);
				extendOrganisation(source,source.HodexID.split(/-/).shift()); // todo: HodexId not implemented yet
			});
			fillTable(resp);
			buildPagination(resp);
			elmTable.classList.remove(classNameHide);
		}
		firstResult();
	}

	/**
	 * Handles the clickEvent on the thead element to sort by term asc or desc.
	 * Causes a new API call.
	 * @param {Event} e
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function handleTheadClick(e){
		if (searchObj) {
			var elmTh = findParent(e.target,'th')
				,dataKey = elmTh.getAttribute('data-key')
				,elmThOld = elmThead.querySelector('.'+classNameSort)
				,isSort = elmThOld===elmTh
			;
			elmThOld&&!isSort&&elmThOld.classList.remove(classNameSort);
			if (isSort)	elmTh.classList.toggle(classNameDesc);
			else				elmTh.classList.add(classNameSort);
			sortTo(dataKey,elmTh.classList.contains(classNameDesc)?classNameDesc:classNameAsc);
		}
	}

	/**
	 * Handles the clickEvent on the tbody element to select a specific result.
	 * @param {Event} e
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function handleTableClick(e){
		var elmTr = findParent(e.target,'tr')
			,hit = elmTr&&getResultById(elmTr.getAttribute('data-id'));
		if (elmTr&&hit) {
			fnBodyClickCallback&&fnBodyClickCallback(hit._source,elmTr);
			details.length&&showResultDetails(elmTr,hit);
		}
	}

	/**
	 * Handles the clickEvent on the pagination list element.
	 * @param {Event|Object} e
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function handlePaginationClick(e){
		var elmLi = findParent(e.target,'li');
		if (elmLi) {
			var nextOrPrev = elmLi.getAttribute('data-do')
				,pageNew
				,pageMax = Math.ceil(searchResult.hits.total/perPage)
			;
			if (nextOrPrev==='prev') {
				pageNew = page-1;
			} else if (nextOrPrev==='next') {
				pageNew = page+1;
			} else {
				pageNew = parseInt(nextOrPrev,10);
			}
			if (pageNew>=0&&pageNew!==page&&pageNew<pageMax) {
				page = pageNew;
				searchObj.body.from = page*perPage;
				doSearch();
			}
		}
	}

	/**
	 * The resize event handler to determine pagination size
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function handleResize(){
		searchResult&&buildPagination(searchResult);
	}

	/**
	 * Handle keyboard 'up' event
	 * @param {KeyboardEvent} e
	 */
	function handleKeyUp(e){
		var keyCode = e.keyCode;
		keyCode===key.LEFT&&handlePaginationClick({target:elmPagination.querySelector('li:first-child *')}) // next
		||keyCode===key.RIGHT&&handlePaginationClick({target:elmPagination.querySelector('li:last-child *')}) // previous
		||keyCode===key.HOME&&handlePaginationClick({target:elmPagination.querySelector('li:nth-child(2) *')}) // first
		||keyCode===key.END&&handlePaginationClick({target:elmPagination.querySelector('li:nth-last-child(2) *')}); // last
	}

	/**
	 * Sends out a new API call.
	 * @param {boolean} [clearNow] Boolean to clear the table immediately, otherwise the table is cleared on callback.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function doSearch(clearNow){
		elmSearchResult.classList.add(classNameSearching);
		clearNow&&clearSearch();
		search(searchObj,handleSearchResult);
	}

	/**
	 * Changes sorting to a specific term and sends an API call.
	 * @param {string} term The term to sort to
	 * @param {string} ascdesc Sort ascending or descending, possible values are 'asc' or 'desc'
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function sortTo(term,ascdesc){
		var sortTerm = {};
		sortTerm[term] = {order:ascdesc};
		searchObj.body.sort = [sortTerm,'_score'];
		doSearch();
	}

	/**
	 * Searches the last search result for a hit with a specific id.
	 * @param {string} id
	 * @returns {Object|undefined} Returns the hit or undefined.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function getResultById(id){
		var hits = searchResult.hits.hits
			,result = undef;
		loop(hits,function(obj){
			obj._id===id&&(result = obj);
		});
		return result;
	}

	/**
	 * Fills the table body with response data.
	 * @param resp
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function fillTable(resp){
		var elmFragment = document.createDocumentFragment();
		loop(resp.hits.hits,appendTableRow.bind(null,elmFragment));
		elmTableBody.appendChild(elmFragment);
	}

	/**
	 * Tests the column widths for the first search result and adds the percentage to thead>tr>th* so that subsequent results have similar column widths.
	 */
	function firstResult(){
		if (!isFirstResult) {
			isFirstResult = !isFirstResult;
			var elmsCols = elmThead.querySelector('tr').childNodes
				,colsNum = elmsCols.length
				,i = colsNum
				,size = []
				,totalWidth;
			while (i--) size[i] = elmsCols[i].offsetWidth;
			totalWidth = size.reduce(function(a,b){return a + b;});
			i = colsNum; while (i--) elmsCols[i].style.width = 100*(size[i]/totalWidth)+'%';
		}
	}

	/**
	 * Builds the pagination list for a response.
	 * @param resp
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function buildPagination(resp){
		var responseHits = resp.hits
			,hits = responseHits.hits
			,hitsNum = hits.length
			,hitsTotal = responseHits.total
		;
		if (hitsTotal>hitsNum) {
			elmPagination.classList.remove(classNameHide); // here, otherwise no offsetWidth
			var elmFragment = createDocumentFragment()
				,numPages = Math.ceil(hitsTotal/perPage)
				//
				,paginationWidth = elmPagination.offsetWidth
				,itemWidth = 30 // mPagination.querySelector('a').offsetWidth
				,maxItems = paginationWidth/itemWidth<<0
				,offset = (maxItems-2)/2<<0
				,posStart = page - offset
				,posEnd = page + offset
				,isFirstPage = page===0
				,isLastPage = page===numPages-1
				,isEdgeStart = false
				,isEdgeEnd = false
			;
			if (posStart<0) {
				posEnd -= posStart;
				posStart = 0;
			}
			if (posEnd>numPages) {
				posStart = Math.max(0,posStart-(posEnd-numPages));
				posEnd = numPages;
			}
			if (posStart!==0) {
				isEdgeStart = true;
				posStart++;
			}
			if (posEnd!==numPages) {
				isEdgeEnd = true;
				posEnd--;
			}
			addPaginationLi(elmFragment,'prev',isFirstPage?['disabled']:undef,'').querySelector('a').appendChild(createElement('span','glyphicon glyphicon-chevron-left'.split(' ')));
			isEdgeStart&&addPaginationLi(elmFragment,0,['edge-left']);
			for (var i=posStart;i<posEnd;i++) addPaginationLi(elmFragment,i);
			isEdgeEnd&&addPaginationLi(elmFragment,numPages-1,['edge-right']);
			addPaginationLi(elmFragment,'next',isLastPage?['disabled']:undef,'').querySelector('a').appendChild(createElement('span','glyphicon glyphicon-chevron-right'.split(' ')));
			//
			elmPagination.innerHTML = '';
			elmPagination.appendChild(elmFragment);
		}
	}

	/**
	 * Adds a pagination list element to a parent node
	 * @param {HTMLElement} parent The parent/fragment to add to
	 * @param {number|string} pageli
	 * @param {Array} [classes]
	 * @param {string} [text]
	 * @returns {HTMLElement}
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function addPaginationLi(parent,pageli,classes,text){
		if (!classes) classes = [];
		if (pageli===page) classes.push('active');
		var elmLi = createElement('li',classes,parent,{'data-do':pageli});
		createElement('a',undef,elmLi,undef,text===undef?pageli+1:text);
		return elmLi;
	}

	/**
	 * Appends a tablerow element for a search hit.
	 * @param {HTMLElement} fragment
	 * @param {esHit} hit
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function appendTableRow(fragment,hit){
		if (hit._type!=='index'){
			var elmTr = createElement('tr',undef,fragment,{'data-id':hit._id});
			for (var i=0;i<columnsNum;i++) {
				var key = columns[i]
					,text = getPropertyContent(hit,key);
				if (key==='OrgUnitId') text = __(text);
				createElement('td',undef,elmTr,undef,text);
			}
		}
	}

	/**
	 * Shows the result details by expanding the table row
	 * @param {HTMLElement} tr The table row to expand
	 * @param {esHit} hit The associated hit result
	 */
	function showResultDetails(tr,hit){
		var elmParent = tr.parentNode
			,elmPreviousInfo = elmParent.querySelector('.info');
		if (elmPreviousInfo) {
			var elmInfoDetails = elmParent.querySelector('.info-details');
			elmInfoDetails&&elmParent.removeChild(elmInfoDetails);
			elmPreviousInfo.classList.remove('info');
		}
		if (elmPreviousInfo!==tr) {
			tr.classList.add('info');
			var elmTrDetails = elmDetailsTarget||createElement('tr',['info','info-details'])
				,elmTdDetails = elmDetailsTarget||createElement('td',undef,elmTrDetails,{colspan:columnsNum})
				,oSource = hit._source;
			setTimeout(function(){
				elmTrDetails.classList.add('reveal');
			},40);
			if (hasAllDetails) {
				elmTrDetails.classList.add('all-details');
				loop(oSource,getDtDd.bind(null,createElement('dl','dl-horizontal',elmTdDetails)));
			} else {
				var elmDl = createElement('dl','dl-horizontal',elmTdDetails);
				loop(details,function(prop){
					var value = getPropertyContent(hit,prop);
					if (prop==='OrgUnitDescription') prop = hit.OrgUnitId;
					getDtDd(elmDl,value,prop,false);
				});
			}
			if (!elmDetailsTarget) {
				var elmNextChild = tr.nextSibling;
				if (elmNextChild) elmParent.insertBefore(elmTrDetails,elmNextChild);
				else elmParent.appendChild(elmTrDetails);
			}
		}
	}

	function getDtDd(dl,dd,dt,deep){
		if (dt&&type(dt)!==type.INT) {
			createElement('dt',undef,dl,undef,__(dt));
		}
		var valueType = type(dd)
			,isString = valueType===type.STRING
			,isArray = valueType===type.ARRAY
			,isArrayInt = isArray&&isIntegerArray(dd)
			,isDeep = deep===undef||deep
			,elmDd = createElement('dd',undef,dl)
		;
		if (isArray&&dd.length===1&&type(dd[0])===type.OBJECT&&isDeep) {
			loop(dd[0],getDtDd.bind(null,dl));
		} else if (valueType===type.OBJECT&&isDeep||isArray&&!isArrayInt&&isDeep) {
			loop(dd,getDtDd.bind(null,createElement('dl','dl-horizontal',elmDd)));
		} else {
			if (dt==='Weblink') createElement('a',undef,elmDd,{href:dd&&dd.indexOf('//')===-1?'//'+dd:dd,target:'_blank'},dd);
			else if (isString&&dd.indexOf(/[\r\n]/gm)) elmDd.innerHTML = dd.replace(/(\r\n|\n|\r)/gm,'<br/>');
			else elmDd.textContent = isArrayInt?dd.join(','):dd;
		}
	}

	function isIntegerArray(a){
		var sum = 0;
		loop(a,function(i){
			sum += i;
		});
		return type(sum)===type.INT;
	}

	/**
	 * Get the property content from a hit.
	 * @param {esHit} hit
	 * @param {string} propName
	 * @returns {string}
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function getPropertyContent(hit,propName){
		return hit._source[propName]||console.warn('property',propName,'not found on',hit)||'['+propName+']';
	}

	/**
	 * Clears the search result from the view
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function clearSearch(){
		elmAlert.classList.add(classNameHide);
		elmTable.classList.add(classNameHide);
		elmTableBody.innerHTML = '';
		elmPagination.classList.add(classNameHide);
		elmPagination.innerHTML = '';
	}

	/**
	 * Removes an element from a parentNode and returns the clone
	 * @param {HTMLElement} ancestor An ancestor element
	 * @param {string} selector A querySelector string
	 * @returns {Node}
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function pickClone(ancestor,selector){
		var elm = ancestor.querySelector(selector)
			,clone = elm.cloneNode(true);
		elm.parentNode.removeChild(elm);
		return clone;
	}

	/**
	 * Sets the textContent of the result heading showing the number of results.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function setSearchResultHeading(){
		var hits = searchResult?searchResult.hits:undef
			,totalHits = hits?hits.total:''
		;
		elmSearchResultHeading.textContent = __('Search result')+' '+totalHits;
	}

	/**
	 * Load CSS
	 * @param {string} href
	 * @param {HTMLElement} [parent]
	 * @param {HTMLElement} [before]
	 * @returns {Promise}
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function loadCSS(href,parent,before){
		if (!parent) parent = document.body;
		return new Promise(function(resolve,reject){
			var elmLink = createElement('link',undef,undef,{
				rel: 'stylesheet'
				,href: href
			});
			elmLink.addEventListener('load',resolve);
			elmLink.addEventListener('error',reject);
			if (before) parent.insertBefore(elmLink,before);
			else parent.appendChild(elmLink);
		});
	}

	/**
	 * Shows the alert element
	 * @param {string} alert The alert message
	 * @param {string} [type='warning'] A type of success, info, warning, or danger.
	 * @memberof sk123ow
	 * @method
	 * @private
	 */
	function showAlert(alert,type){
		if (elmAlert) {
			if (!type) type = 'warning';
			for (var i=0,l=elmAlert.classList.length;i<l;i++) {
				var className = elmAlert.classList[i];
				if (className!=='alert') elmAlert.classList.remove(className);
			}
			elmAlert.classList.add('alert-'+type);
			elmAlert.querySelector('span.text').textContent = alert;
			elmAlert.classList.remove(classNameHide);
		}
	}

	/**
	 * Maps localisation to a key
	 * @param {collectionItem[]} langSource ie InstitutionLanguageStrings or CourseLanguageStrings
	 * @param {String} langKey the key to set
	 * @param {String} sourceKey the key to lookup in langSource
	 */
	function languageMapping(langSource,langKey,sourceKey){
		if (langSource) {
			loop(langSource,function(/**collectionItem*/langObj){
				addLang(langObj.Language,langKey,langObj[sourceKey]);
			});
		}
	}

	/**
	 * Return a 'must' object for a query
	 * @param {string} field
	 * @param {string} value
	 * @returns {Object}
	 */
	function getMatch(field,value){
		var fieldSplit = field.split('.')
			,isNested = fieldSplit.length>1
			,match = {};
		match[field] = value;
		return isNested?{
				nested:{
					path: fieldSplit[0]
					,query: {
						bool: {
							must: [
								{match:match}
							]
						}
					}
				}
			}:{match:match};
	}

	/**
	 * Extend/flatten a source object by Language iso
	 * @param {hitCourse} source
	 * @param {String} languageExpandKey
	 * @param {String} iso
	 */
	function extendIso(source,languageExpandKey,iso){
		if (source.hasOwnProperty(languageExpandKey)) {
			var /**collectionItem[]*/collection = source[languageExpandKey]
				,current = collection[0];
			loop(collection,function(item){
				if (item.Language===iso) current = item;
			});
			extend(source,current);
		}
	}

	/**
	 * Extend/flatten a source object by nested keys
	 * @param {hitCourse} source
	 * @param {object} mappings
	 * @param {string} type
	 */
	function extendExpands(source,mappings,type){
		mappings.hasOwnProperty(type)&&loop(mappings[type],function(value,key){
			source.hasOwnProperty(key)&&extend(source,source[key][0]);
		});
	}

	/**
	 * Extend/flatten a source object by organisation
	 * @param {hitCourse} source
	 * @param {String} orgUnitId
	 */
	function extendOrganisation(source,orgUnitId){
		organisations.hasOwnProperty(orgUnitId)&&extend(source,organisations[orgUnitId]);
	}

	return extend(init,{
		alert: showAlert
		,version: version
	});
})();