/**
 * Error handling for the Studiekeuze123 Opleidingen Widget
 * @namespace sk123ow.error
 */
window.sk123ow.error = (function(){
	'use strict';

	var l18n = iddqd.l18n
		,__ = l18n.__
		,string = iddqd.internal.native.string
		,sprintf = string.sprintf
		,alert = sk123ow.alert
	;

	/**
	 * Handle all errors
	 * @param {object|string|Event} err
	 */
	function error(err){
		var sMessage
			,aWarn;
		if (err.constructor===Event) {
			var mTarget = err.target
				,sTargetType = mTarget&&mTarget.nodeName.toLowerCase()
				,sError = {link:'errorCSSLoad',script:'errorScriptLoad'}[sTargetType]||''
				,sFile = mTarget&&mTarget.getAttribute({link:'href',script:'src'}[sTargetType]||'')
			;
			sMessage = sprintf(__(sError),sFile);
		} else if (err.hasOwnProperty('message')) {
			sMessage = __(err.message);
		} else {
			sMessage = __(err);
		}
		aWarn = [sMessage,err];
		if (err.hasOwnProperty('stack')) aWarn.push(err.stack);
		console.warn.apply(console,aWarn);
		alert(sMessage,'danger');
		//if (err.message==='No Living connections') {
		//	initElasticsearch();
		//}
	}

	return iddqd.extend(error,{
		INVALID_KEY: 'invalidKey'
		,ONE_INSTANCE: 'oneInstance'
		,LOAD_ERROR: 'loadError'
	});
})();