//  * @mixes courseExpands
//  * @extends courseExpands
//  * @typedef {Object} courseExpands
//  * @mixin courseExpands
//  * @extends hitCourse
/**
 * An ElasticSearch course hit
 * @typedef {Object} hitCourse
 * @property {boolean} ActiveProgram
 * @property {String} City
 * @property {String} CourseId
 * @property {number} CrohoCode
 * @property {number} CrohoVariant
 * @property {boolean} Dual
 * @property {String} Duration
 * @property {boolean} ErasmusMundusProgram
 * @property {number} FieldOfEducation
 * @property {String} Financing
 * @property {boolean} FullTime
 * @property {String} HasHonoursProgram
 * @property {String} HodexID
 * @property {String} Instelling_SK123
 * @property {String} Instelling_SK123ID
 * @property {String} NumerusFixus
 * @property {String} OrgUnitId
 * @property {boolean} PartTime
 * @property {String} PercentageDistanceLearning
 * @property {String} PercentageForeignStudents
 * @property {String} PercentageForeignTeachers
 * @property {String} PercentageNightEducation
 * @property {String} PercentageStudentsAbroad
 * @property {String} PercentageTeachersAbroad
 * @property {String} ProgramCredits
 * @property {String} ProgramLevel
 * @property {String} Qualification
 * @property {String} Searchwords
 * @property {String} SizeOfTutorialGroups
 * @property {String} SourceUrl
 * @property {String} StudyChoiceCheckDescription
 * @property {String} StudyChoiceCheckName
 * @property {String} StudyChoiceCheckPolicyOfAdmissionAfter1May
 * @property {String} StudyChoiceCheckPolicyOfAdmissionBefore1May
 * @property {String} StudyChoiceCheckRequired
 * @property {String} StudyChoiceCheckStudyChoiceCheckWebLink
 * @property {String} YearlyStudyCosts
 *
 * @mixes courseExpands
 *   @property {courseLanguageStrings[]} CourseLanguageStrings
 *   @property {orgUnitLocation[]} OrgUnitLocations
 *
 * @mixes courseLanguageStrings
 *   @property {String} CourseId "752"
 *   @property {String} CourseLanguageStringId "1571"
 *   @property {String} CourseObjectives null
 *   @property {String} Faculty
 *   @property {String} Language "nl"
 *   @property {String} ProgramDescription
 *   @property {String} ProgramName
 *   @property {String} ProgramSummary
 *   @property {String} StudySubject
 *   @property {String} Weblink
 * 
 * @mixes orgUnitLocation
 *   @property {String} Location "Maastricht"
 *   @property {String} OrgUnitLocationId "756"
 */

/** Course expands
 * @typedef {Object} courseExpands
 * @property {courseLanguageStrings[]} CourseLanguageStrings
 * @property {orgUnitLocation[]} OrgUnitLocations
 */

/**
 * A CourseLanguageStrings item
 * @typedef {Object} courseLanguageStrings
 * @property {String} CourseId "752"
 * @property {String} CourseLanguageStringId "1571"
 * @property {String} CourseObjectives null
 * @property {String} Faculty
 * @property {String} Language "nl"
 * @property {String} ProgramDescription
 * @property {String} ProgramName
 * @property {String} ProgramSummary
 * @property {String} StudySubject
 * @property {String} Weblink
 */

/**
 * A InstitutionLanguageStrings item
 * @typedef {Object} institutionLanguageStrings
 * @property {String} InstitutionId "5"
 * @property {String} InstitutionLanguageStringId "7"
 * @property {String} Language "nl"
 * @property {String} OrgUnitDescription
 * @property {String} OrgUnitName
 * @property {String} OrgUnitSummary
 */

/**
 * An OrgUnitLocation
 * @typedef {Object} orgUnitLocation
 * @property {String} CourseId "752"
 * @property {String} Location "Maastricht"
 * @property {String} OrgUnitLocationId "756"
 */

/**
 * A collection with a language
 * @typedef {Object} collectionItem
 * @property {String} Language
 */

// ELASTICSEARCH

/**
 * An ElasticSearch result
 * @typedef {Object} esResult
 * @property {esShards} _shards
 * @property {Object<esAggregation>} aggregations
 * @property {esHits} hits
 * @property {boolean} timed_out
 * @property {number} took
 */

/**
 * An ElasticSearch shards result
 * @typedef {Object} esShards
 * @property {number} failed
 * @property {number} successful
 * @property {number} total
 */

/**
 * An ElasticSearch hit collection
 * @typedef {Object} esHits
 * @property {esHit[]} hits
 * @property {number} max_score
 * @property {number} total
 */

/**
 * An ElasticSearch hit
 * @typedef {Object} esHit
 * @property {String} _id
 * @property {String} _index
 * @property {number} _score
 * @property {Object} _source
 * @property {String} _type
 */

/**
 * An ElasticSearch msearch result
 * @typedef {Object} esResultM
 * @property {esResult[]} responses
 */

/**
 * An ElasticSearch aggregation item
 * @typedef {Object} esAggregation
 * @property {esBucketItem[]} buckets
 * @property {number} doc_count_error_upper_bound
 * @property {number} sum_other_doc_count
 */

/**
 * An ElasticSearch bucket item
 * @typedef {Object} esBucketItem
 * @property {number} doc_count
 * @property {String} key
 */
