/*global process*/
var connect = require('connect')
	,serveStatic = require('serve-static')
	,openBrowser = require('open')
	,roots = process.argv[2]||''
	,matchFile = roots.match(/[^/]*\.[^/]*$/)
	,file = matchFile&&matchFile.pop()||''
	,root = process.argv[2]&&roots.replace(file,'').replace(/\/$/,'')||'src'
	,port = process.argv[3]||8181
;
console.log('root',root);
console.log('file',file);
console.log('port',port);

connect()
    .use(serveStatic('./'+root+'/'))
    .listen(port);

openBrowser('http://localhost:'+port+'/'+file);