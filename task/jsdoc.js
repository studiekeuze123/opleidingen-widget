var promisify = require('promisify-node')
	,rimraf = promisify(require('rimraf'))
	// ,exec = promisify(require('child_process').exec)
	,utils = require(__dirname+'/util/utils')
	,read = utils.read
	,save = utils.save
	,copy = utils.copy
	//,log = utils.log
	,warn = utils.warn
	,exec = utils.exec
	,querySelector = utils.querySelector
	//
	,sourceDocParams = './doc/sk123ow.html'
	,sourceDocIndex = './doc/index.html'
;

rimraf('./doc/*')
  .then(exec.bind(null,'grunt prepare',{cwd:'./jsdoc'}),warn)
  .then(exec.bind(null,'"node_modules/.bin/jsdoc" -c jsdoc.json',{cwd:'./'}),warn)
	// .then(log,warn)
  // .then(docChild=>{
  //   return new Promise((resolve,reject)=>{
  //     docChild.on('close',resolve);
  //     docChild.on('error',reject);
  //   });
  // })
  .then(copy.bind(null,'./dist/sk123ow.min.js','./doc/scripts/sk123ow.min.js'),warn)
  .then(extendDoc,warn)
  .then(()=>console.log('documentation generated'),warn)
;

function extendDoc(){
	return read(sourceDocParams)
    .then(querySelector.bind(null,'table.params table.params'))
    .then(select=>Array.prototype.map.call(select.querySelectorAll('tbody tr'),tr=>{
			var content = Array.prototype.map.call(tr.querySelectorAll('td'),td=>td.textContent.replace(/[\s\n]+/g,' ').replace(/^\s|\s$/g,''));
			return {
				name: content[0]
				,type: content[1]
				,arg: content[2]
				,default: content[3]
				,description: content[4]
			};
		}))
		.then(params=>{
			return read(sourceDocIndex)
				.then(indexSource=>{
					var options = '';
					params.forEach(param=>{
						options += '<dt id="option-'+param.name+'"><a href="#option-'+param.name+'"">'+param.name+'</a><small class="option-type" title="type">'+param.type+'</small><small class="option-default" title="default: '+param.default+'">'+param.default+'</small></dt>';
						options += '<dd>'+param.description+'</dd>';
					});
					return indexSource.replace(/{{options}}/,'<dl class="dl-horizontal options">'+options+'</dl>');
				});
		})
		.then(save.bind(null,sourceDocIndex));
}