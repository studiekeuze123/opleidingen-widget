module.exports = (uri,injection)=>{
	return new Promise((resolve,reject)=>{
		require('child_process').exec(['phantomjs',injection,uri].join(' '),(error,stdout)=>{
			if (error) reject([error,stdout]);
			else resolve(stdout);
		});
	});
}