/**
 * Indexes Institutions and Courses from oData to Elasticsearch
 * @see: http://www.odata.org/
 * @see: https://github.com/elastic/elasticsearch-js
 * @see: https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/api-reference.html
 */
var request = require('request')
	,elasticsearch = require('elasticsearch')
	,commander = require('commander')
			.usage('[options] <files ...>')
			.option('--config [source=local]', 'Config file')
			.parse(process.argv)
	,config = require(__dirname+'/.config.'+(commander.config||'local'))
	//
	,searchParams = {$format:'json'}
	//
	,headers = {'content-type': 'application/x-www-form-urlencoded'}
	//
	// ,log = console.log.bind(console)
	,warn = console.warn.bind(console)
	//
	// elasticsearch data
	,esOptions = {index:config.index}
	,esClient = new elasticsearch.Client({
		host: config.host
		,requestTimeout: config.requestTimeout
		// ,log: 'trace'
	})
	,es = {
		index: promisify(esClient,esClient.index)
		,indicesExists: promisify(esClient,esClient.indices.exists)
		,indicesCreate: promisify(esClient,esClient.indices.create)
		,indicesDelete: promisify(esClient,esClient.indices.delete)
		,indicesPutMapping: promisify(esClient,esClient.indices.putMapping)
	}
;

Promise.all([
	requestToken().then(json=>setHeaderAuthorisation(json.access_token),warn).then(()=>console.log('oAuth header token set'))
	,es.indicesExists()
		.then(exists=>exists?es.indicesDelete():Promise.resolve(),warn)
		.then(es.indicesCreate.bind(esClient,{allow_primary:true}))
		.then(es.indicesPutMapping.bind(esClient,config.mapping))
		.then(()=>console.log('Elasticsearch index \''+config.index+'\' purged'))
])
	.then(()=>Promise.all([

		// collect Institutions
		requestData('Institutions',config.expandInstitutions&&{$expand:config.expandInstitutions}||{})
			.then(
				result=>Promise.all(result.value.map(inst=>es.index({
					type: 'organisation'
					,id: inst.OrgUnitId
					,body: inst
				})))
				,warn)
			.then(indexed=>console.log('Institutions indexed: '+indexed.length),warn)

		// collect Courses
		,requestData('Courses',config.expandCourses&&{$expand:config.expandCourses}||{})
			.then(
				result=>Promise.all(result.value.map(course=>es.index({
					type: 'program'
					,id: course.CourseId
					,body: course
				})))
				,warn)
			.then(indexed=>console.log('Courses indexed: '+indexed.length),warn)

		// collect ...
		/*,requestData('',{})
			.then(result=>{
				console.log('result...',result.value.map(o=>o.url));
			},warn)*/

	]))
	.then(()=>console.log('Indexing finished'),warn)
;

/**
 * A promisified request
 * @param {object} options
 * @param {boolean} json
 * @returns {Promise} Resolves to a JSON object
 */
function promiseRequest(options,json){
	if (json===undefined) json = true;
	return  new Promise((resolve,reject)=>{
		request(options, (err,result)=>err&&reject(err)||result&&resolve(json?JSON.parse(result.body):result.body)||reject(err));
	});
}

/**
 * Request a token
 * @returns {Promise}
 */
function requestToken(){
	return promiseRequest({
		url: config.uriToken
		,method: 'POST'
		,headers: headers
		,form: {
			'grant_type': 'client_credentials'
			,'client_secret': config.clientSecret
			,'client_id': config.clientId
		}
	});
}

/**
 * Set the access_token to the Authorization header
 * @param {string} token
 */
function setHeaderAuthorisation(token){
	headers.Authorization = 'bearer '+token;
}

/**
 * A request to the REST API
 * @param {string} endpoint
 * @param {object} params
 * @returns {Promise}
 */
function requestData(endpoint,params){
	console.log('requestData',config.uriApi+(endpoint?'/'+endpoint:'')+paramise(params)); // todo: remove log
	return promiseRequest({
		url: config.uriApi+(endpoint?'/'+endpoint:'')+paramise(params)
		,method: 'GET'
		,headers: headers
	});
}

/**
 * Returns a url search parameter string
 * @param {object} params
 * @returns {String}
 */
function paramise(params){
	var fullParams = extend(params||{},searchParams)
		,a = []
		,search;
	for(var s in fullParams) a.push(s+'='+fullParams[s]);
	search = a.join('&');
	return search===''?search:'/?'+search;
}

/**
 * Extend an object
 * @name iddqd.extend
 * @method
 * @param {object} target Subject.
 * @param {object} source Property object.
 * @param {boolean} [overwrite=false]  Overwrite properties.
 * @returns {object} Subject.
 */
function extend(target,source,overwrite){
	for (var s in source) {
		if (overwrite||target[s]===undefined) {
			target[s] = source[s];
		}
	}
	return target;
}

/**
 * Promisify a regular function with signature (options,callback)
 * @param {object} [thisArg]
 * @param {Function} method
 * @returns {Promise}
 */
function promisify(thisArg,method){
	return options=>new Promise((resolve,reject)=>{
		method.call(thisArg,extend(options||{},esOptions),(error,response)=>error&&reject(error)||resolve(response));
	});
}

/*
(Courses+Institutions)*(CourseLanguageStrings+InstitutionLanguageStrings)

/
	https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/?$format=json
		odata.metadata:"https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/$metadata"
		value:[
			0:{name: "Instellingen", url: "Instellingen"}
			1:{name: "Opleidingsvormen", url: "Opleidingsvormen"}
			2:{name: "Vestigingen", url: "Vestigingen"}
			3:{name: "Opleidingen", url: "Opleidingen"}
			4:{name: "InstellingOpleidingen", url: "InstellingOpleidingen"}
			5:{name: "Studies", url: "Studies"}
			6:{name: "Gemeenten", url: "Gemeenten"}
			7:{name: "Doorstroom", url: "Doorstroom"}
			8:{name: "Accreditations", url: "Accreditations"}
			9:{name: "Afstudeerrichtingen", url: "Afstudeerrichtingen"}
			10:{name: "ApplicationRequirements", url: "ApplicationRequirements"}
			11:{name: "Beroepen", url: "Beroepen"}
			12:{name: "Collegegelden", url: "Collegegelden"}
			13:{name: "Courses", url: "Courses"}
			14:{name: "CourseContacts", url: "CourseContacts"}
			15:{name: "CourseLanguageStrings", url: "CourseLanguageStrings"}
			16:{name: "InstructionLanguages", url: "InstructionLanguages"}
			17:{name: "InstructionModes", url: "InstructionModes"}
			18:{name: "OpenDagen", url: "OpenDagen"}
			19:{name: "OpenDagDoelgroepNiveaus", url: "OpenDagDoelgroepNiveaus"}
			20:{name: "OpenDagFases", url: "OpenDagFases"}
			21:{name: "OpenDagLocaties", url: "OpenDagLocaties"}
			22:{name: "OpenDagNiveauOpleidingen", url: "OpenDagNiveauOpleidingen"}
			23:{name: "OpenDagOpleidingen", url: "OpenDagOpleidingen"}
			24:{name: "OrgUnitLocations", url: "OrgUnitLocations"}
			25:{name: "ProgramForms", url: "ProgramForms"}
			26:{name: "ProgramRuns", url: "ProgramRuns"}
			27:{name: "ProgramStarts", url: "ProgramStarts"}
			28:{name: "Scholarships", url: "Scholarships"}
			29:{name: "Searchwords", url: "Searchwords"}
			30:{name: "StudyAdvices", url: "StudyAdvices"}
			31:{name: "StudyCosts", url: "StudyCosts"}
			32:{name: "Tracks", url: "Tracks"}
			33:{name: "Tuitionfees", url: "Tuitionfees"}
			34:{name: "Varianten", url: "Varianten"}
			35:{name: "ContactData", url: "ContactData"}
			36:{name: "Institutions", url: "Institutions"}
			37:{name: "InstitutionLanguageStrings", url: "InstitutionLanguageStrings"}

/Institutions
	https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/Institutions(2L)/?$format=json
		Instelling: "Avans Hogeschool"
		InstellingEngels: "Avans University of Applied Sciences"
		Instelling_SK123ID: "182"
		InstitutionId: "2"
		OrgUnitId: "avans"
		OrgUnitType: "University of Applied Sciences"
		SourceUrl: "http://studiedatabase.nl/hodex/avans/orgUnitData.xml"
		odata.metadata: "https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/$metadata#Institutions/@Element"
	https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/Institutions(2L)/?$expand=InstitutionLanguageStrings&$format=json
		* (above result extended by)
		InstitutionLanguageStrings:[{InstitutionLanguageStringId: "1", Language: "nl", OrgUnitName: "Avans Hogeschool",…},…]
			0:{
				InstitutionLanguageStringId: "1", Language: "nl", OrgUnitName: "Avans Hogeschool",…}
				InstitutionId:"2"
				InstitutionLanguageStringId:"1"
				Language:"nl"
				OrgUnitDescription:"Avans heeft locaties in Breda, 's-Hertogenbosch en Tilburg.↵↵In 2004 is Avans Hogeschool ontstaan uit Hogeschool Brabant en Hogeschool `s-Hertogenbosch. Deze hogescholen werkten al langer nauw samen, onder één College van Bestuur."
				OrgUnitName:"Avans Hogeschool"
				OrgUnitSummary:"Avans Hogeschool behoort met 29.000 studenten, ruim 100 hbo-opleidingen (voltijd, deeltijd, duaal en Associate degrees) en 2.600 medewerkers tot de grote hogescholen van Nederland."
			1:{
				InstitutionLanguageStringId: "2", Language: "en", OrgUnitName: "Avans University of Applied Sciences",…}
				InstitutionId:"2"
				InstitutionLanguageStringId:"2"
				Language:"en"
				OrgUnitDescription:"The basic principle of Avans University of Applied Sciences is rooted in and springs from its social environment."
				OrgUnitName:"Avans University of Applied Sciences"
				OrgUnitSummary:null

/Courses
	https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/Courses(999L)
		ActiveProgram:true
		City:null
		CourseId:"999"
		CrohoCode:66826
		CrohoVariant:0
		Dual:false
		Duration:"12 (maand)"
		ErasmusMundusProgram:false
		FieldOfEducation:2411
		Financing:"government"
		FullTime:true
		HasHonoursProgram:false
		HodexID:"uvt-3066"
		Instelling_SK123:"Tilburg University"
		Instelling_SK123ID:"57"
		NumerusFixus:null
		PartTime:false
		PercentageDistanceLearning:"0%"
		PercentageForeignStudents:null
		PercentageForeignTeachers:null
		PercentageNightEducation:"0"
		PercentageStudentsAbroad:null
		PercentageTeachersAbroad:null
		ProgramCredits:"60.00"
		ProgramLevel:"WO master"
		Qualification:"Master of Science"
		SizeOfTutorialGroups:null
		SourceUrl:"http://studiedatabase.nl/hodex/programs/uvt/3066"
		StudyChoiceCheckDescription:null
		StudyChoiceCheckName:null
		StudyChoiceCheckPolicyOfAdmissionAfter1May:null
		StudyChoiceCheckPolicyOfAdmissionBefore1May:null
		StudyChoiceCheckRequired:null
		StudyChoiceCheckStudyChoiceCheckWebLink:null
		YearlyStudyCosts:null
		odata.metadata:"https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/$metadata#Courses/@Element"
	https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc/Courses(999L)/?$expand=CourseLanguageStrings&$format=json
		* (above result extended by)
		CourseLanguageStrings:[{CourseLanguageStringId: "2087", CourseId: "999", Language: "nl",…},…]
			0:{
				CourseLanguageStringId: "2087", CourseId: "999", Language: "nl",…}
				CourseId:"999"
				CourseLanguageStringId:"2087"
				CourseObjectives:null
				Faculty:"Tilburg School of Humanities"
				Language:"nl"
				ProgramDescription:"De master Communicatie- en Informatiewetenschappen kent 5 tracks: Bedrijfscommunicatie en Digitale Media; Communicatie-design; Data Journalism; Human Aspects of Information Technology en Data Science: Business and Governance.↵De masteropleiding Communicatie- en Informatiewetenschappen is een opleiding voor studenten die geïnteresseerd zijn in verschillende disciplinaire perspectieven op menselijke communicatie. Tijdens de opleiding verdiep je je in de taalkundige, psychologische en multimediale aspecten van communicatie. Het is een erg interessante en brede opleiding. Het onderzoek bevat vele disciplines, die bijna allemaal vertegenwoordigd zijn in het onderwijs. Bij Tilburg University (School of Humanities) worden door taalkundigen, sociale wetenschappers of marketingdeskundigen colleges gegeven. Het unieke van de opleiding in Tilburg University is dat wij als enige de koppeling leggen tussen vergevorderde kennis over technologie en de expertise over hoe de mensen hiermee omgaan.↵↵"
				ProgramName:"Communicatie- en Informatiewetenschappen"
				ProgramSummary:"De master kent 5 tracks, te weten Bedrijfscommunicatie en Digitale Media; Communicatie-design; Human Aspects of Information Technology; Data Journalism en Data Science: Business and Governance"
				StudySubject:"Kernvakken: Social Media at work & play; Communication strategy; Advertising & persuasion; Multimodality & communication.↵↵http://mystudy.uvt.nl/it10.owp?taal=e&pfac=FGW&variantcode=2Q319&ajaar=2013&minorcode= "
				Weblink:"http://www.tilburguniversity.edu/nl/onderwijs/masteropleidingen/communicatie-en-informatiewetenschappen/"
			1:{
				CourseLanguageStringId: "2088", CourseId: "999", Language: "en",…}
				CourseId:"999"
				CourseLanguageStringId:"2088"
				CourseObjectives:"The Master in Communication & Information Sciences offers a unique programme where you learn about ways how people inform themselves and others, and how new means of communication can be used in a creative way. More information: http://mystudy.uvt.nl/it10.owp?taal=e&pfac=FGW&variantcode=2Q319&ajaar=2013&minorcode="
				Faculty:"Tilburg School of Humanities"
				Language:"en"
				ProgramDescription:"This program is intended for highly talented and motivated students with a keen sense of intellectual curiosity and intellectual ambition. In this Master's program, you will encounter state of the art research in the broad interdisciplinary field of communication.↵↵The tracks:↵Business Communication and Digital Media: focus on communication issues in the area of business communications↵↵Communication Design: Communication designers research different factors that influence the effectiveness of texts as communication tools↵↵Human Aspect of Information Technology: How can we make sure that people get the exact information or (access to) the knowledge that they require↵↵Data Journalism: explore the opportunities of data journalism from four angles: data retrieval, data storytelling, data visualization and data publishing.↵↵Data Science: Business and Governance: trains students to become Data Scientists. The Harvard Business Review calls it "the sexiest job of the 21st century"!↵↵"
				ProgramName:"Communication and Information Sciences"
				ProgramSummary:"The MSc in Communication and Information Sciences offers a unique program in which you learn about how people acquire/transfer information and how new means of communication can be used innovatively."
				StudySubject:"A selection of subjects: Communication strategies; Language, culture & identity; Negotiation strategies; Culture, communication and computers;  Knowledge management; Non-verbal communication; Language Policy; Social Media; Organizational Decision Making and Cooperation; Evolution of language.↵http://mystudy.uvt.nl/it10.owp?taal=e&pfac=FGW&variantcode=2Q319&ajaar=2013&minorcode= "
				Weblink:"http://www.tilburguniversity.edu/education/masters-programmes/communication-and-information-sciences/"
*/
