/*global phantom,require*/
(function(page){
	'use strict';
	var system = require('system')
		,args = system.args
		,sSrcUri = args[1]
	;

	page.viewportSize = { width: 800, height: 600 };
	page.onLoadFinished = handleLoadFinished;
	page.open(sSrcUri);

	function handleLoadFinished(){
		var sHtml = page.evaluate(function() {
			var mHTML = document.body;
			Array.prototype.forEach.call(mHTML.querySelectorAll('[remContent]'),function(elm){
				while (elm.childNodes.length) {
					elm.removeChild(elm.childNodes[0]);
				}
				elm.removeAttribute('remContent');
			});
			Array.prototype.forEach.call(mHTML.querySelectorAll('[rem]'),function(elm){
				elm.parentNode.removeChild(elm);
			});
			return mHTML.innerHTML;
		});
		console.log(sHtml);
		phantom.exit(sHtml);
	}

})(require('webpage').create());

