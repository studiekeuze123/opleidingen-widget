var glob = require('glob')
	,fs = require('fs')
	,mkdirp = require('mkdirp');

function glomise(globstring) {
	return new Promise(function (resolve) {//reject
		glob(globstring,function(err,result){
			resolve(result);
		});
	});
}

function read(file){
	return new Promise((resolve,reject)=>
		fs.readFile(file,(err,data)=>
			err?reject(err):resolve(data.toString())
			// err?(reject(err),process.exit(1)):resolve(data.toString())
		)
	);
}

function save(file,data) {
	console.log('saving',file);
	return new Promise(function(resolve,reject){
		mkdirp(getDirName(file), function(err) {
			err&&reject(err);
			fs.writeFile(file, data, resolve);
		});
	});
}

function copy(source, target) {
	console.log('copying',source,'to',target);
	return new Promise(function(resolve,reject){
		var cbCalled = false;

		var readStream = fs.createReadStream(source);
		readStream.on('error', done);

		var writeStream = fs.createWriteStream(target);
		writeStream.on('error', done);
		writeStream.on('close',()=>done());
		readStream.pipe(writeStream);

		function done(err) {
			if (!cbCalled) {
				err&&reject(err)||resolve();
				cbCalled = true;
			}
		}
	});
}


/*function copy(source,target){
	return read(source).then(save.bind(null,target));
}*/

function getDirName(file){
	return file.replace(/[^\/\\]*\.\w{0,4}$/,'');
}

function querySelector(selector,source){
	return new Promise((resolve,reject)=>require('jsdom').env(source,[],(err,window)=>err&&reject(err)||resolve(window.document.querySelector(selector))));
}

function throwError(msg){
	return ()=>{
		var error = new Error(msg||'Unspecified error.');
		throw error;
		// return error;
	};
}

function exec(cmd, opts) {
	opts || (opts = {});
	return new Promise((resolve,reject)=>{
		const child = require('child_process').exec(cmd,opts,(err,stdout,stderr)=>err?reject(err):resolve({
				stdout: stdout,
				stderr: stderr
		}));
    child.on('close',resolve);
    child.on('error',reject);
		opts.stdout&&child.stdout.pipe(opts.stdout);
		opts.stderr&&child.stderr.pipe(opts.stderr);
	});
}

module.exports = {
	glomise
	,read
	,save
	,copy
	,querySelector
	,throwError
	,exec
	,log: console.log.bind(console)
	,warn: console.warn.bind(console)
};