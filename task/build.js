var childProcess = require('child_process')
    ,tasks = (tasks=>tasks.length?tasks:['copy','less','uglify'])(process.argv.slice(2))
    ,promise = Promise.resolve();

tasks.forEach(task=>{
  promise = promise.then(runScript.bind(null,task[0]==='+'?'npm run '+task.substr(1):'./task/'+task));
});
promise.then(()=>console.log('build success'),console.warn.bind(console,'build failed'));

function runScript(scriptPath) {
  //console.log('\t'+scriptPath); // todo: remove log
	return new Promise(function(resolve,reject){
    var invoked = false
        ,process = scriptPath.match(/\s/)&&childProcess.exec(scriptPath)||childProcess.fork(scriptPath)
      ;
    process.on('error',err=>{
        if (invoked) return;
        invoked = true;
        reject(err);
    });
    process.on('exit',(code,signal)=>{
        if (invoked) return;
        invoked = true;
        var err = code === 0 ? null : new Error('exit code ' + code);
        err&&reject(err)||resolve(err);
    });
    process.on('message',(m)=>{
       console.log(arguments);
    });
	});
}