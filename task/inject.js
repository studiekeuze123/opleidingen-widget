console.log('building'); // todo: remove log

var phantomInject = require(__dirname+'/phantomInject')
	,utils = require(__dirname+'/util/utils')
	,minify = require('html-minifier').minify
	,less = require('less')
	,path = require('path')
	,read = utils.read
	,save = utils.save
	,warn = console.warn.bind(console)
	//
	,sourceWidget = './src/widget.html'
	,sourceStripWidget = './task/util/phantomStripWidget.js'
	,sourceJS = './src/js/sk123ow.js'
	//
	,sourceLess = './src/style/main.less'
;

phantomInject(sourceWidget,sourceStripWidget)
	.then(result=>minify(result,{
		removeComments: true,
		removeCDATASectionsFromCDATA: true,
		removeIgnored: true,
		removeOptionalTags: true,
		collapseWhitespace: true
	}),console.warn.bind(console))
	.then(source=>insertVars(sourceJS,/(,html\s?=\s?)(.*)/g,'$1\''+source+'\''),warn)
	.then(save.bind(null,sourceJS),warn)
	.then(()=>console.log('widget HTML injected'))
;

read(sourceLess)
	.then(parseLess.bind(null,path.resolve(sourceLess)),warn)
	.then(source=>insertVars(sourceJS,/(,css\s?=\s?)(.*)/g,'$1\''+source.replace(/'/g,'\\\'')+'\''),warn)
	.then(save.bind(null,sourceJS),warn)
	.then(()=>console.log('widget CSS injected'))
;

function insertVars(file,regex,replacement){
	return read(file)
		.then(source=>source.replace(regex,replacement));
}

function parseLess(filename,data){
	return new Promise(function(resolve,reject){
		less.render(data,{
			compress: true
			,filename: filename
		}, function (err, output) {
			if (err) reject(err);
			else resolve(output.css);
		});
	});
}