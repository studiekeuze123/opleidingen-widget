var uglify = require('uglify-js')
	,htmlparser = require('htmlparser2')
	,commander = require('commander')
			.usage('[options] <files ...>')
			.option('--source [source]', 'Source html')
			.option('--target [target]', 'Target js')
			.parse(process.argv)
	,utils = require(__dirname+'/util/utils')
	,read = utils.read
	,save = utils.save
	,warn = utils.warn
	,warnxit = msg=>{
		console.warn(msg);
		process.exit(1);
	}
	// ,throwError = utils.throwError
	,source = commander.source
	,target = commander.target
;

read(source)
	.then(parseScriptSrc,warnxit)
	// .then(parseScriptSrc,warn)
	// .then(parseScriptSrc,throwError('File \''+source+'\' not found'))
	.then(files=>Promise.all(files.map(file=>'./src'+file).map(read)),warn)
	.then(sourceJS=>minify(sourceJS.join(';')),warn)
	.then(save.bind(null,target),warn);

function parseScriptSrc(htmlSource){
	var scripts = []
		,parser = new htmlparser.Parser({
				onopentag: (name,attribs)=>{
					var src = attribs.src;
					name==='script'&&src&&scripts.push(src);
				}
			}, {decodeEntities: true});
	parser.write(htmlSource);
	parser.end();
	return scripts;
}

function minify(source){
	return uglify.minify(source, {
		fromString: true
		,pure_getters: true
	}).code;
}