var commander = require('commander')
			.usage('[options] <files ...>')
			.option('--config [source=local]', 'Config file')
			.parse(process.argv)
	,config = require(__dirname+'/.config.'+(commander.config||'local'));

require('child_process').exec(
	'elasticsearch'
	,{cwd:'./'+config.folder+'/bin/'}
	,(error, stdout, stderr)=>{
		console.log(stdout);
		error&&console.warn(stderr);
	}
);