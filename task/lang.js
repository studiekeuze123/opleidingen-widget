var promisify = require('promisify-node')
	,glob = promisify(require('glob'))
	,utils = require(__dirname+'/util/utils')
	,read = utils.read
	,save = utils.save
  ,warn = console.warn.bind(console)
	//
	,source = './lang/*.po'
	,target = './src/js/sk123ow.lang.iso.js'
	,targetSource = 'window.sk123ow.lang.iso=string;'
;

glob(source)
	.then(files=>files.filter(file=>file.indexOf('sk123ow-base')===-1),warn)
	.then(files=>Promise.all(files.map(read).concat(Promise.resolve(files))))
	.then(allResult=>{
		allResult.pop().forEach((file,i)=>{
			var contents = allResult[i].split(/\r?\n/g)
				,iso = file.match(/\-([^.]+)/).pop()
				,translations = walk(contents);
			save(target.replace('iso',iso),targetSource.replace('iso',iso).replace('string',JSON.stringify(translations)));
		});
	})
	.then(console.log.bind(console),warn)
;

function walk(list){
	var result = {}
		,nextKey = 0
		,matchKey
		,key;
	list.forEach(line=>{
		try {
			if (/^#:\s.+/.test(line)) {
				nextKey = 1;
			} else if (nextKey===1) {
				matchKey = line.match(/^msgid "(.+)"$/);
				if (matchKey) {
					key = matchKey.pop();
					nextKey = 2;
				}
			} else if (nextKey===2) {
				result[key] = line.match(/^msgstr "(.*)"$/).pop();
				nextKey = 0;
			}
		}catch(err){
			console.log('err',line,err); // todo: remove log
		}
	});
	return result;
}