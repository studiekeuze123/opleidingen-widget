var notAnalyzed = {
	type:'string',
	index:'not_analyzed'
};
module.exports = {
	// oData oAuth2
	uriToken: 'https://token-acceptatie.studiekeuzedatabase.nl/token'
	,uriApi: 'https://api-acceptatie.studiekeuzedatabase.nl/DatamartService.svc'
	,clientId: 'Test'
	,clientSecret: 'VN5/YG8lI8uo76wXP6tC+39Z1Wzv+XTI/bc0LPLP40U='
	// Elasticsearch
	,folder: 'elasticsearch-2.3.2'
	,index: 'map-test1'
	,host: 'localhost:9200'
	,requestTimeout: 1E6
	,expandInstitutions: 'InstitutionLanguageStrings'
	,expandCourses: 'CourseLanguageStrings,OrgUnitLocations,InstructionLanguages,Searchwords'
	,mapping: {
		type: 'program'
		,body: {
			program:{
				properties:{
					ProgramLevel: notAnalyzed,
					Qualification: notAnalyzed,
					OrgUnitLocations: {
						type:'nested',
						properties: {
							Location: notAnalyzed
						}
					},
					InstructionLanguages: {
						type:'nested',
						properties: {
							LanguageCode: notAnalyzed
						}
					}
				}
			}
		}
	}
};